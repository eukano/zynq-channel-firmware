/*
    Copyright 2019 Ethan Reesor

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// send.sv: Reads outgoing packets from memory

`timescale 1ns / 1ps

module send #(
        parameter BOUNDARY = axi::BURST_32_BIT + 8 // MODIFY FOR TESTING ONLY
    )(
        input wire clock,
        input wire resetn,

        axi_full.reader memory,            // AXI Master
        queue.reader    q_send_descriptor, // Descriptor Queue
        queue.writer    q_send_status,     // Status Queue
        stream.writer   m_stream           // Stream Master
    );

    enum {IDLE, ACTIVE, ERROR} state;
    reg [31:0] descriptor_address, next_descriptor_address;
    driver::descriptor packet, next_packet;
    assign next_packet = driver::as_descriptor(q_send_descriptor.rdata[0+:driver::DESCRIPTOR_BITS]);
    assign next_descriptor_address = q_send_descriptor.rdata[driver::DESCRIPTOR_BITS+:32];

    reg [15:0] arremain, rremain, txremain;
    reg        queue_valid;
    reg [31:0] queue_data;

    task read_packet(input logic [31:0] packet_address, input logic [15:0] packet_length, remaining);
        automatic reg [16:0] length = remaining;
        automatic reg [31:0] address = packet_address + packet_length - length;
        automatic reg [31:0] boundary = address[31:BOUNDARY] + 1;

        // ensure no single read crosses a 1kB boundary
        if ((boundary << BOUNDARY) - address < length) begin
            length = (boundary << BOUNDARY) - address;
            arremain <= remaining - length;
        end else begin
            arremain <= 0;
        end

        // ensure address is 32-bit aligned
        if (address[1:0] != 0) begin
            length = length + address[1:0];
            address[1:0] = 0;
        end

        // ensure length is 32-bit aligned
        if (length[1:0] != 0) begin
            length = length + (4 - length[1:0]);
        end
        
        if (remaining == packet_length)
            rremain <= length;
        else
            rremain <= rremain + length;

        // begin transaction
        memory.read_address(1, address, axi::BURST_INCR, axi::BURST_32_BIT, length / 4, axi::NORMAL_NOCACHE, axi::UNPRIV_SECURE_DATA);
    endtask

    task send_packet();
        automatic reg [ 1:0] offset = packet.address[1:0];
        automatic reg [ 2:0] length = 4;
        automatic reg [ 3:0] keep = 'hF;
        automatic reg        last = 'bX;
        automatic reg [31:0] qdata = queue_data;
        automatic reg        qhas = queue_valid;
        automatic reg [31:0] mdata = memory.rdata;
        automatic reg        mhas = memory.rbeat();

        // handle partial packets
        if (txremain == packet.length) begin
            length = length - offset;
            keep = keep >> offset;
        end

        if (txremain < length) begin
            keep = keep &~ ((1 << (length - txremain)) - 1);
            length = txremain;
        end

        // decrement read remaining
        if (mhas)
            rremain <= rremain - 4;

        // check read response
        last = txremain <= (qhas ? 4 : 0) + (mhas ? 4 : 0);
        if (mhas && (memory.rresp != axi::RESP_OKAY || (memory.rlast && rremain <= 4) ^ last)) begin
            state <= ERROR;
            memory.rready <= rremain < 4;
            q_send_status.push({descriptor_address, packet.status | driver::TRANSFER_ERROR});

            if (~m_stream.txbusy())
                m_stream.txsend(1, packet.channel, 'hX, 'h0, 1);

            return;
        end

        // send data
        if (~m_stream.txbusy() && (qhas || mhas)) begin
            if (qhas) begin
                qhas = mhas;
                qdata = mdata;
                
            end else
                mhas = 0;
            
            m_stream.txsend(1, packet.channel, mdata, keep, txremain <= 4);
            txremain <= txremain - length;
        end

        // queue data
        if (mhas && ~qhas) begin
            qhas = 1;
            mhas = 0;
            qdata = mdata;
        end

        // ready for more?
        memory.rready <= ~(qhas || last);
        queue_valid <= qhas;
        queue_data <= queue_data;
    endtask

    always @(posedge clock or negedge resetn) begin
        q_send_descriptor.rready <= 0;
        q_send_status.wvalid <= 0;

        if (~resetn) begin
            state <= IDLE;
            memory.arvalid <= 0;
            memory.rready <= 0;
            m_stream.txvalid <= 0;
            queue_valid <= 0;

        end else begin
            // check for next packet
            if (state == IDLE && q_send_descriptor.rvalid) begin
                state <= ACTIVE;
                packet <= next_packet;
                descriptor_address <= next_descriptor_address;
                q_send_descriptor.pop();

                txremain <= next_packet.length;
                read_packet(next_packet.address, next_packet.length, next_packet.length);
                queue_valid <= 0;
                memory.rready <= 1;
            end

            // clear on complete TX transaction
            if (m_stream.txbeat())
                m_stream.txvalid <= 0;

            // clear or start next on complete AR transaction
            if (memory.arbeat()) begin
                if (arremain == 0 || state == ERROR)
                    memory.arvalid <= 0;
                else
                    read_packet(packet.address, packet.length, arremain);
            end

            if (state == ACTIVE && txremain > 0)
                send_packet();

            if (state == ACTIVE && txremain == 0 && ~m_stream.txbusy() && q_send_status.wready) begin
                state <= IDLE;
                q_send_status.push({descriptor_address, packet.status | driver::TRANSFER_COMPLETE});
            end

            if (state == ERROR) begin
                // terminate the stream
                if (~m_stream.txlast && ~m_stream.txbusy())
                    m_stream.txsend(1, packet.channel, 'hX, 'h0, 1);

                // wait for read to complete
                if (memory.rbeat()) begin
                    rremain <= rremain - 4;
                    memory.rready <= rremain < 4;
                end

                if (rremain == 0 && m_stream.txlast && ~m_stream.txbusy())
                    state <= IDLE;
            end
        end
    end

endmodule