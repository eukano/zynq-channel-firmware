/*
    Copyright 2019 Ethan Reesor

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// receive_test.sv: Tests for the receive module

`timescale 1ns / 1ps

module receive_test();
    import test::*;

    reg clock, resetn;
    enum {RESET, UNALIGNED, PAGES, SHORT, LONG, DONE} test;
    reg [31:0] index, value, address, length;

    initial clock = 1;
    always #1 clock = ~clock;

    initial resetn = 0;
    always #10 resetn = 1;

    initial begin
        test = RESET;
        memory.arvalid = 0;
        memory.rready = 0;
        q_recv_descriptor.wvalid <= 0;
        q_recv_status.rready <= 0;
        m_stream.rxready <= 0;
        m_stream.ryvalid <= 0;

        resetn = 0;
        @(posedge clock);
        resetn = 1;

        // test_unaligned();
        // test_pages();
        // test_short();
        test_long();
        complete();
    end

    localparam MEM_OFFSET = aximem.mem.OFFSET;
    localparam DESC_ADDR = 'h120 + MEM_OFFSET;
    localparam PACK_ADDR = 'h320;
    localparam PACK_CHAN = 'h9;
    localparam PACK_ID = 'hA;
    localparam PACK_LEN = 'h20;
    localparam PACK_STATUS = 'h5A5A5A00;

    localparam integer PACK_DATA [0:7] = '{
        'h2b1ee54e,
        'h802a8c1a,
        'h92673584,
        'hb4fd462c,
        'hcf71386a,
        'h8633ee0c,
        'hcdc278ff,
        'h098f30d6
    };

    task test_unaligned();
        test = UNALIGNED;
        $display("Testing unaligned packet");

        aximem.mem.M[PACK_ADDR/4+0] = 0;
        aximem.mem.M[PACK_ADDR/4+1] = 0;
        aximem.mem.M[PACK_ADDR/4+2] = 0;
        aximem.mem.M[PACK_ADDR/4+3] = 0;
        aximem.mem.M[PACK_ADDR/4+4] = 0;
        aximem.mem.M[PACK_ADDR/4+5] = 0;
        aximem.mem.M[PACK_ADDR/4+6] = 0;
        aximem.mem.M[PACK_ADDR/4+7] = 0;

        m_stream.rxready = 1;
        q_recv_descriptor.push({
            DESC_ADDR,
            driver::descriptor_bits('{
                address: PACK_ADDR + MEM_OFFSET + 1,
                channel: PACK_CHAN,
                id: PACK_ID,
                length: PACK_LEN - 2,
                status: PACK_STATUS
            })
        });

        @(posedge clock);
        q_recv_descriptor.wvalid <= 0;

        while (~m_stream.rxbeat())
            @(posedge clock);

        assert_equal(m_stream.rxchan, PACK_CHAN, "RXCHAN incorrect");
        m_stream.rxready <= 0;

        for (int i = 0; i < 8; i++) begin
            m_stream.rysend(1, PACK_DATA[i], {i > 0, 2'b11, i < 7}, i == 7);

            @(posedge clock);
            while (~m_stream.rybeat())
                @(posedge clock);
        end

        m_stream.ryvalid <= 0;

        while (~q_recv_status.rvalid)
            @(posedge clock);

        assert_equal(q_recv_status.rdata[63:32], DESC_ADDR, "Descriptor address invalid");
        assert_equal(q_recv_status.rdata[31: 0], PACK_STATUS | driver::TRANSFER_COMPLETE, "Packet status invalid");

        q_recv_status.pop();
        @(posedge clock);
        q_recv_status.rready <= 0;

        assert_equal(aximem.mem.M[PACK_ADDR/4+0][23:0], PACK_DATA[0][23:0], "Memory invalid (0)");
        assert_equal(aximem.mem.M[PACK_ADDR/4+1], PACK_DATA[1], "Memory invalid (1)");
        assert_equal(aximem.mem.M[PACK_ADDR/4+2], PACK_DATA[2], "Memory invalid (2)");
        assert_equal(aximem.mem.M[PACK_ADDR/4+3], PACK_DATA[3], "Memory invalid (3)");
        assert_equal(aximem.mem.M[PACK_ADDR/4+4], PACK_DATA[4], "Memory invalid (4)");
        assert_equal(aximem.mem.M[PACK_ADDR/4+5], PACK_DATA[5], "Memory invalid (5)");
        assert_equal(aximem.mem.M[PACK_ADDR/4+6], PACK_DATA[6], "Memory invalid (6)");
        assert_equal(aximem.mem.M[PACK_ADDR/4+7][31:8], PACK_DATA[7][31:8], "Memory invalid (7)");
    endtask

    task test_pages();
        test = PAGES;
        $display("Test multi-page packet");

        address = 1 << (receive.BOUNDARY - 1);
        length = 1 << (receive.BOUNDARY + 1);
        index = 'h01020304;

        m_stream.rxready = 1;
        q_recv_descriptor.push({
            DESC_ADDR,
            driver::descriptor_bits('{
                address: PACK_ADDR + MEM_OFFSET + 1,
                channel: PACK_CHAN,
                id: PACK_ID,
                length: length - 2,
                status: PACK_STATUS
            })
        });

        @(posedge clock);
        q_recv_descriptor.wvalid <= 0;

        while (~m_stream.rxbeat())
            @(posedge clock);

        assert_equal(m_stream.rxchan, PACK_CHAN, "RXCHAN incorrect");
        m_stream.rxready <= 0;


        forever begin
            value = index[31:24];
            m_stream.rysend(1, index, {value > 1, 2'b11, value < length/4}, value == length/4);

            @(posedge clock);
            while (~m_stream.rybeat())
                @(posedge clock);

            if (value == length/4)
                break;

            value = index[7:0] + 1;
            index = {index[23:0], value[7:0]};
        end

        m_stream.ryvalid <= 0;

        while (~q_recv_status.rvalid)
            @(posedge clock);

        assert_equal(q_recv_status.rdata[63:32], DESC_ADDR, "Descriptor address invalid");
        assert_equal(q_recv_status.rdata[31: 0], PACK_STATUS | driver::TRANSFER_COMPLETE, "Packet status invalid");

        q_recv_status.pop();
        @(posedge clock);
        q_recv_status.rready <= 0;
    endtask

    task test_short();
        test = SHORT;
        $display("Testing packet with too-short stream");

        m_stream.rxready = 1;
        q_recv_descriptor.push({
            DESC_ADDR,
            driver::descriptor_bits('{
                address: PACK_ADDR + MEM_OFFSET,
                channel: PACK_CHAN,
                id: PACK_ID,
                length: PACK_LEN,
                status: PACK_STATUS
            })
        });

        @(posedge clock);
        q_recv_descriptor.wvalid <= 0;

        while (~m_stream.rxbeat())
            @(posedge clock);

        assert_equal(m_stream.rxchan, PACK_CHAN, "RXCHAN incorrect");
        m_stream.rxready <= 0;

        m_stream.rysend(1, 'hDEADBEEF, 4'hF, 1);

        @(posedge clock);
        while (~m_stream.rybeat())
            @(posedge clock);

        m_stream.ryvalid <= 0;

        while (~q_recv_status.rvalid)
            @(posedge clock);

        assert_equal(q_recv_status.rdata[63:32], DESC_ADDR, "Descriptor address invalid");
        assert_equal(q_recv_status.rdata[31: 0], PACK_STATUS | driver::TRANSFER_ERROR, "Packet status invalid");

        q_recv_status.pop();
        @(posedge clock);
        q_recv_status.rready <= 0;

        while (receive.state != receive.IDLE)
            @(posedge clock);
    endtask

    task test_long();
        test = LONG;
        $display("Testing packet with too-long stream");

        m_stream.rxready = 1;
        q_recv_descriptor.push({
            DESC_ADDR,
            driver::descriptor_bits('{
                address: PACK_ADDR + MEM_OFFSET,
                channel: PACK_CHAN,
                id: PACK_ID,
                length: 4,
                status: PACK_STATUS
            })
        });

        @(posedge clock);
        q_recv_descriptor.wvalid <= 0;

        while (~m_stream.rxbeat())
            @(posedge clock);

        assert_equal(m_stream.rxchan, PACK_CHAN, "RXCHAN incorrect");
        m_stream.rxready <= 0;

        for (int i = 0; i < 8; i++) begin
            m_stream.rysend(1, 'hDEADBEEF, 4'hF, i == 7);

            @(posedge clock);
            while (~m_stream.rybeat())
                @(posedge clock);
        end

        m_stream.ryvalid <= 0;

        while (~q_recv_status.rvalid)
            @(posedge clock);

        assert_equal(q_recv_status.rdata[63:32], DESC_ADDR, "Descriptor address invalid");
        assert_equal(q_recv_status.rdata[31: 0], PACK_STATUS | driver::TRANSFER_ERROR, "Packet status invalid");

        q_recv_status.pop();
        @(posedge clock);
        q_recv_status.rready <= 0;

        while (receive.state != receive.IDLE)
            @(posedge clock);
    endtask

    task complete();
        test = DONE;
        $display("Tests complete");
        @(posedge clock);
        $finish();
    endtask

    axi_full memory();
    queue #(.WIDTH(128)) q_recv_descriptor();
    queue #(.WIDTH(64)) q_recv_status();
    stream m_stream();

    axi_memory aximem (.*, .A(memory));
    flat_queue #(.WIDTH(128)) descriptor_impl(.*, .q(q_recv_descriptor));
    flat_queue #(.WIDTH(64)) status_impl(.*, .q(q_recv_status));

    receive #(.BOUNDARY(axi::BURST_32_BIT + 4)) receive(.*);

endmodule