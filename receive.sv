/*
    Copyright 2019 Ethan Reesor

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// receive.sv: Writes incoming packets to memory

`timescale 1ns / 1ps

module receive #(
        parameter BOUNDARY = axi::BURST_32_BIT + 8 // MODIFY FOR TESTING ONLY
    )(
        input wire clock,
        input wire resetn,

        axi_full.writer memory,            // AXI Master
        queue.reader    q_recv_descriptor, // Descriptor Queue
        queue.writer    q_recv_status,     // Status Queue
        stream.reader   m_stream           // Packet Slave
    );

    enum {IDLE, ACTIVE, ERROR} state;
    reg [31:0] descriptor_address, next_descriptor_address;
    driver::descriptor packet, next_packet;
    assign next_packet = driver::as_descriptor(q_recv_descriptor.rdata[0+:driver::DESCRIPTOR_BITS]);
    assign next_descriptor_address = q_recv_descriptor.rdata[driver::DESCRIPTOR_BITS+:32];

    reg [15:0] awremain, wremain, bremain;
    reg [55:0] queue_data;
    reg [ 2:0] queue_depth;

    function [2:0] axi_wants(input logic [15:0] remaining);
        if (remaining < packet.length) begin
            // middle or end
            axi_wants = remaining < 4 ? remaining : 4;

        end else begin
            // beginning
            axi_wants = 4 - packet.address[1:0];
            if (remaining < axi_wants)
                axi_wants = remaining;
        end
    endfunction

    task write_packet(input logic [31:0] packet_address, input logic [15:0] packet_length, remaining);
        automatic reg [16:0] length = remaining;
        automatic reg [31:0] address = packet_address + packet_length - length;
        automatic reg [31:0] boundary = address[31:BOUNDARY] + 1;

        // ensure no single write crosses a 1kB boundary
        if ((boundary << BOUNDARY) - address < length) begin
            length = (boundary << BOUNDARY) - address;
            awremain <= remaining - length;
        end else begin
            awremain <= 0;
        end

        bremain <= length;

        // ensure address is 32-bit aligned
        if (address[1:0] != 0) begin
            length = length + address[1:0];
            address[1:0] = 0;
        end

        // ensure length is 32-bit aligned
        if (length[1:0] != 0) begin
            length = length + (4 - length[1:0]);
        end

        // begin transaction
        memory.write_address(1, address, axi::BURST_INCR, axi::BURST_32_BIT, length / 4, axi::NORMAL_NOCACHE, axi::UNPRIV_SECURE_DATA);
    endtask

    task receive_packet();
        automatic reg [ 2:0] want = axi_wants(wremain);
        automatic reg [ 2:0] count = want;
        automatic reg [ 1:0] offset = 'hX;
        automatic reg [31:0] data = 'hX;
        automatic reg [ 3:0] strobe = 0;
        automatic reg [55:0] qdata = queue_data;
        automatic reg [ 2:0] qhas = queue_depth;
        automatic reg [31:0] tdata = 'hX;
        automatic reg [ 2:0] thas = 0;

        // compress stream bytes
        if (m_stream.rybeat()) begin
            for (integer i = 0; i < 4; i++)
                if (m_stream.rykeep[i]) begin
                    tdata[thas*8 +: 8] = m_stream.rydata[i*8 +: 8];
                    thas = thas + 1;
                end
        end

        if (memory.wbusy()) begin
            m_stream.ryready <= 0;
            count = 0;

        end else if (qhas + thas >= want) begin
            // got enough data
            wremain <= wremain - want;
            bremain <= bremain - want;

            // dequeue bytes
            for (integer i = 0; i < 4; i++)
                if (qhas > 0 && want > 0) begin
                    data = {qdata[7:0], data[31:8]};
                    strobe = {1'b1, strobe[3:1]};
                    qdata = {8'hX, qdata[55:8]};
                    want = want - 1;
                    qhas = qhas - 1;
                end

            // consume stream bytes
            for (integer i = 0; i < 4; i++)
                if (thas > 0 && want > 0) begin
                    data = {tdata[7:0], data[31:8]};
                    strobe = {1'b1, strobe[3:1]};
                    tdata = {8'hX, tdata[31:8]};
                    want = want - 1;
                    thas = thas - 1;
                end

            if (wremain == packet.length) begin
                // beginning of packet
                offset = packet.address[1:0];

                // apply offset
                for (integer i = 0; i < 4; i++)
                    if (offset > 0) begin
                        data = {8'hX, data[31:8]};
                        strobe = {1'b0, strobe[3:1]};
                        offset = offset - 1;
                    end
            end

            // send data
            memory.write_data(1, data, strobe, bremain <= 4);
            m_stream.ryready <= 0;

        end else begin
            m_stream.ryready <= 1;
            count = 0;
        end

        // enqueue bytes
        for (integer i = 0; i < 4; i++)
            if (thas > 0) begin
                qdata[qhas*8 +: 8] = tdata[7:0];
                tdata = {8'hX, tdata[31:8]};
                qhas = qhas + 1;
                thas = thas - 1;
            end

        // update queue
        queue_data <= qdata;
        queue_depth <= qhas;

        if (qhas <= 3)
            m_stream.ryready <= 1;

        if (bremain <= qhas + count)
            m_stream.ryready <= 0;

        if (m_stream.rybeat())
            if (m_stream.rylast ^ (wremain == qhas + count)) begin
                state <= ERROR;
                q_recv_status.push({descriptor_address, packet.status | driver::TRANSFER_ERROR});
                m_stream.ryready <= ~m_stream.rylast;
                memory.bready <= ~m_stream.rylast;
            end
    endtask

    always @(posedge clock or negedge resetn) begin
        q_recv_descriptor.rready <= 0;
        q_recv_status.wvalid <= 0;

        if (~resetn) begin
            state <= IDLE;
            memory.awvalid <= 0;
            memory.wvalid <= 0;
            memory.bready <= 0;
            m_stream.rxvalid <= 0;
            m_stream.ryready <= 0;
            queue_depth <= 0;

        end else begin
            // check for next packet
            if (state == IDLE && q_recv_descriptor.rvalid) begin
                state <= ACTIVE;
                packet <= next_packet;
                descriptor_address <= next_descriptor_address;
                q_recv_descriptor.pop();

                wremain <= next_packet.length;
                write_packet(next_packet.address, next_packet.length, next_packet.length);
                m_stream.rxsend(1, next_packet.channel);
            end

            // clear on complete RX transaction
            if (m_stream.rxbeat())
                m_stream.rxvalid <= 0;

            // wait for AW to finish
            if (state == ACTIVE && memory.awbeat()) begin
                memory.awvalid <= 0;
                m_stream.ryready <= 1;
            end

            // process data
            if (state == ACTIVE && ~memory.awbusy() && bremain > 0) begin
                // clear on complete RY transaction
                if (m_stream.rybeat())
                    m_stream.ryready <= 0;

                receive_packet();
            end

            // wait for response
            if (state == ACTIVE && ~memory.awbusy() && bremain == 0) begin
                if (memory.wbeat()) begin
                    memory.wvalid <= 0;
                    memory.bready <= 1;
                end

                if (memory.bbeat()) begin
                    memory.bready <= 0;

                    if (awremain == 0) begin
                        state <= IDLE;

                        if (memory.bresp == axi::RESP_OKAY)
                            q_recv_status.push({descriptor_address, packet.status | driver::TRANSFER_COMPLETE});
                        else
                            q_recv_status.push({descriptor_address, packet.status | driver::TRANSFER_ERROR});

                    end else begin
                        write_packet(packet.address, packet.length, awremain);
                    end
                end
            end

            // cleanup
            if (state == ERROR) begin
                if (m_stream.rybeat() && m_stream.rylast)
                    m_stream.ryready <= 0;

                if (memory.wbeat())
                    memory.wvalid <= 0;

                if (memory.bbeat())
                    memory.bready <= 0;

                if (bremain > 0 && ~memory.wbusy()) begin
                    memory.write_data(1, 'hX, 'h0, bremain <= 4);
                    bremain <= bremain - 4;

                    if (bremain <= 4)
                        memory.bready <= 1;
                end

                if (bremain == 0 && ~m_stream.ryready && ~memory.wvalid && ~memory.bready)
                    state <= IDLE;
            end
        end
    end

endmodule