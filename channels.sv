/*
    Copyright 2019 Ethan Reesor

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// channels.sv: Top-level module

`timescale 1ns / 1ps

module channels #(
        QUEUE_DEPTH = 16
    ) (
        input wire clock,
        input wire resetn,

        axi_lite.slave  s_axi_lite, // Register AXI Slave
        axi_full        m_axi_desc, // Descriptor AXI Master
        axi_full        m_axi_pack, // Packet AXI Master
        packet.slave    s_packet,   // Packet Slave
        packet.master   m_packet,   // Packet Master
        stream          m_stream,   // Stream Master

        output reg introut
    );

    /*
     * Descriptor:
     *   - Address (32-bit)
     *   - Channel (8-bit)
     *   - ID (8-bit)
     *   - Length (16-bit)
     *   - Status (32-bit)
     *
     * Queues:
     *   - Address - Address of Descriptor (32-bit)
     *   - Descriptor - Address of Descriptor and Descriptor (128-bit)
     *   - Status - Address of Descriptor and Descriptor Status (64-bit)
     *   - Complete - Address of Descriptor (32-bit)
     *
     * Flow:
     *   1. Enqueue: register write => address queue
     *   2. Fetch: address queue => load descriptor => descriptor queue
     *   3. Execute
     *      + Send: descriptor queue => memory to stream => status queue
     *      + Receive: descriptor queue => stream to memory => status queue
     *   4. Finalize: status queue => store descriptor status => complete queue
     *   5. Dequeue: complete queue => register read
     */

    localparam ADDR_R00_MAIN_CONTROL   = 8'h00; // control
    localparam ADDR_R01_MAIN_STATUS    = 8'h01; // status

    localparam ADDR_R10_PACKET_STATUS  = 8'h10; // packet status
    localparam ADDR_R11_PACKET_MESSAGE = 8'h11; // enqueue outgoing messages, dequeue incoming messages

    localparam ADDR_R20_SEND_STATUS    = 8'h20; // send status
    localparam ADDR_R21_SEND_ENQUEUE   = 8'h21; // enqueue descriptors to be sent
    localparam ADDR_R22_SEND_DEQUEUE   = 8'h22; // dequeue sent descriptors
    localparam ADDR_R23_SEND_ABORT     = 8'h23; // aborted outgoing descriptors

    localparam ADDR_R30_RECV_STATUS    = 8'h30; // receive status
    localparam ADDR_R31_RECV_ENQUEUE   = 8'h31; // enqueue descriptors to be received
    localparam ADDR_R32_RECV_DEQUEUE   = 8'h32; // dequeue received descriptors
    localparam ADDR_R33_RECV_ABORT     = 8'h33; // aborted incoming descriptors

    localparam POS_PACKET_OUTGOING_DEPTH = 'h00;
    localparam POS_PACKET_INCOMING_DEPTH = 'h04;
    localparam POS_PACKET_INTR_OUTGOING  = 'h10;
    localparam POS_PACKET_INTR_INCOMING  = 'h11;

    localparam POS_SEND_ADDRESS_DEPTH    = 'h00;
    localparam POS_SEND_COMPLETE_DEPTH   = 'h04;
    localparam POS_SEND_ABORT_DEPTH      = 'h08;
    localparam POS_SEND_INTR_START       = 'h10;
    localparam POS_SEND_INTR_ABORT       = 'h11;
    localparam POS_SEND_INTR_DONE        = 'h12;

    localparam POS_RECV_ADDRESS_DEPTH    = 'h00;
    localparam POS_RECV_COMPLETE_DEPTH   = 'h04;
    localparam POS_RECV_ABORT_DEPTH      = 'h08;
    localparam POS_RECV_INTR_START       = 'h10;
    localparam POS_RECV_INTR_ABORT       = 'h11;
    localparam POS_RECV_INTR_DONE        = 'h12;

    reg [31:0] control, main_status, packet_status, send_status, recv_status;
    reg intr_packet_outgoing, intr_packet_incoming;
    reg intr_send_start, intr_send_abort, intr_send_done;
    reg intr_recv_start, intr_recv_abort, intr_recv_done;

    wire [$clog2(QUEUE_DEPTH):0] packet_outgoing_depth, packet_incoming_depth, send_address_depth, recv_address_depth, send_complete_depth, recv_complete_depth, send_abort_depth, recv_abort_depth;

    assign introut = intr_packet_outgoing || intr_packet_incoming || intr_send_start || intr_send_abort || intr_send_done || intr_recv_start || intr_recv_abort || intr_recv_done;

    always @* begin
        packet_status = 0;
        packet_status[POS_PACKET_OUTGOING_DEPTH+:4] = packet_outgoing_depth;
        packet_status[POS_PACKET_INCOMING_DEPTH+:4] = packet_incoming_depth;
        packet_status[POS_PACKET_INTR_OUTGOING]     = intr_packet_outgoing;
        packet_status[POS_PACKET_INTR_INCOMING]     = intr_packet_incoming;

        send_status = 0;
        send_status[POS_SEND_ADDRESS_DEPTH+:4]  = send_address_depth;
        send_status[POS_SEND_COMPLETE_DEPTH+:4] = send_complete_depth;
        send_status[POS_SEND_ABORT_DEPTH+:4]    = send_abort_depth;
        send_status[POS_SEND_INTR_START]        = intr_send_start;
        send_status[POS_SEND_INTR_ABORT]        = intr_send_abort;
        send_status[POS_SEND_INTR_DONE]         = intr_send_done;

        recv_status = 0;
        recv_status[POS_RECV_ADDRESS_DEPTH+:4]  = recv_address_depth;
        recv_status[POS_RECV_COMPLETE_DEPTH+:4] = recv_complete_depth;
        recv_status[POS_RECV_ABORT_DEPTH+:4]    = recv_abort_depth;
        recv_status[POS_RECV_INTR_START]        = intr_recv_start;
        recv_status[POS_RECV_INTR_ABORT]        = intr_recv_abort;
        recv_status[POS_RECV_INTR_DONE]         = intr_recv_done;
    end

    function bit_set(input [31:0] wdata, input [3:0] wstrb, input [4:0] pos);
        bit_set = wstrb[pos/8] && wdata[pos];
    endfunction

    task clear_packet_intr(input [31:0] wdata, input [3:0] wstrb);
        if (bit_set(wdata, wstrb, POS_PACKET_INTR_OUTGOING)) intr_packet_outgoing <= 0;
        if (bit_set(wdata, wstrb, POS_PACKET_INTR_INCOMING)) intr_packet_incoming <= 0;
    endtask

    task clear_send_intr(input [31:0] wdata, input [3:0] wstrb);
        if (bit_set(wdata, wstrb, POS_SEND_INTR_START)) intr_send_start <= 0;
        if (bit_set(wdata, wstrb, POS_SEND_INTR_ABORT)) intr_send_abort <= 0;
        if (bit_set(wdata, wstrb, POS_SEND_INTR_DONE)) intr_send_done <= 0;
    endtask

    task clear_recv_intr(input [31:0] wdata, input [3:0] wstrb);
        if (bit_set(wdata, wstrb, POS_RECV_INTR_START)) intr_recv_start <= 0;
        if (bit_set(wdata, wstrb, POS_RECV_INTR_ABORT)) intr_recv_abort <= 0;
        if (bit_set(wdata, wstrb, POS_RECV_INTR_DONE)) intr_recv_done <= 0;
    endtask


    /**************************************************\
    |*                   Interfaces                   *|
    \**************************************************/

    load_intr load_intr();
    store_intr store_intr();

    queue #(.WIDTH(32)) q_packet_outgoing();
    queue #(.WIDTH(32)) q_packet_incoming();
    queue #(.WIDTH(32)) q_send_address();
    queue #(.WIDTH(32)) q_recv_address();
    queue #(.WIDTH(128)) q_send_descriptor();
    queue #(.WIDTH(128)) q_recv_descriptor();
    queue #(.WIDTH(64)) q_send_status();
    queue #(.WIDTH(64)) q_recv_status();
    queue #(.WIDTH(32)) q_send_complete();
    queue #(.WIDTH(32)) q_recv_complete();
    queue #(.WIDTH(32)) q_send_abort();
    queue #(.WIDTH(32)) q_recv_abort();


    /**************************************************\
    |*                    Modules                     *|
    \**************************************************/

    load load (.*, .intr(load_intr), .memory(m_axi_desc));
    send send (.*, .memory(m_axi_pack));
    receive receive (.*, .memory(m_axi_pack));
    store store (.*, .intr(store_intr), .memory(m_axi_desc));

    control_queue packet_outgoing_queue(.*, .q(q_packet_outgoing), .depth(packet_outgoing_depth));
    control_queue packet_incoming_queue(.*, .q(q_packet_incoming), .depth(packet_incoming_depth));
    control_queue send_address_queue(.*, .q(q_send_address), .depth(send_address_depth));
    control_queue recv_address_queue(.*, .q(q_recv_address), .depth(recv_address_depth));
    control_queue send_complete_queue(.*, .q(q_send_complete), .depth(send_complete_depth));
    control_queue recv_complete_queue(.*, .q(q_recv_complete), .depth(recv_complete_depth));
    control_queue send_abort_queue(.*, .q(q_send_abort), .depth(send_abort_depth));
    control_queue recv_abort_queue(.*, .q(q_recv_abort), .depth(recv_abort_depth));

    flat_queue #(.WIDTH(128)) send_descriptor_queue(.*, .q(q_send_descriptor));
    flat_queue #(.WIDTH(128)) recv_descriptor_queue(.*, .q(q_recv_descriptor));
    flat_queue #(.WIDTH(64)) send_status_queue(.*, .q(q_send_status));
    flat_queue #(.WIDTH(64)) recv_status_queue(.*, .q(q_recv_status));


    /**************************************************\
    |*                    Packets                     *|
    \**************************************************/

    // incoming
    assign q_packet_incoming.wvalid = s_packet.pvalid;
    assign s_packet.pready = q_packet_incoming.wready;
    assign q_packet_incoming.wdata = driver::message_bits('{channel: s_packet.pchan, unused: 'hX, direction: s_packet.pdir, typ: s_packet.ptype, length: s_packet.plen});

    // outgoing
    assign m_packet.pvalid = q_packet_outgoing.rvalid;
    assign q_packet_outgoing.rready = m_packet.pready;

    driver::message m_message;
    always @* begin
        m_message = driver::as_message(q_packet_outgoing.rdata);
        m_packet.pchan = m_message.channel;
        m_packet.pdir = m_message.direction;
        m_packet.ptype = m_message.typ;
        m_packet.plen = m_message.length;
    end


    /**************************************************\
    |*                 Register: Read                 *|
    \**************************************************/

    always @(posedge clock or negedge resetn) begin
        q_packet_incoming.rready <= 0;
        q_send_complete.rready <= 0;
        q_recv_complete.rready <= 0;
        q_send_abort.rready <= 0;
        q_recv_abort.rready <= 0;

        if (~resetn) begin
            s_axi_lite.arready <= 1;
            s_axi_lite.rvalid <= 0;

        end else begin
            if (s_axi_lite.arbeat()) begin
                s_axi_lite.arready <= 0;
                s_axi_lite.rvalid <= 1;

                case (s_axi_lite.araddr)
                    {ADDR_R00_MAIN_CONTROL, 2'b0}: begin
                        s_axi_lite.rdata <= control;
                        s_axi_lite.rresp <= axi::RESP_OKAY;
                    end

                    {ADDR_R01_MAIN_STATUS, 2'b0}: begin
                        s_axi_lite.rdata <= main_status;
                        s_axi_lite.rresp <= axi::RESP_OKAY;
                    end

                    {ADDR_R10_PACKET_STATUS, 2'b0}: begin
                        s_axi_lite.rdata <= packet_status;
                        s_axi_lite.rresp <= axi::RESP_OKAY;
                    end

                    {ADDR_R11_PACKET_MESSAGE, 2'b0}: if (q_packet_incoming.rvalid) begin
                        s_axi_lite.rresp <= axi::RESP_OKAY;
                        s_axi_lite.rdata <= q_packet_incoming.pop();
                    end else begin
                        s_axi_lite.rresp <= axi::RESP_SLVERR;
                    end

                    {ADDR_R20_SEND_STATUS, 2'b0}: begin
                        s_axi_lite.rdata <= send_status;
                        s_axi_lite.rresp <= axi::RESP_OKAY;
                    end

                    {ADDR_R22_SEND_DEQUEUE, 2'b0}: if (q_send_complete.rvalid) begin
                        s_axi_lite.rresp <= axi::RESP_OKAY;
                        s_axi_lite.rdata <= q_send_complete.pop();
                    end else begin
                        s_axi_lite.rresp <= axi::RESP_SLVERR;
                    end

                    {ADDR_R23_SEND_ABORT, 2'b0}: if (q_send_abort.rvalid) begin
                        s_axi_lite.rresp <= axi::RESP_OKAY;
                        s_axi_lite.rdata <= q_send_abort.pop();
                    end else begin
                        s_axi_lite.rresp <= axi::RESP_SLVERR;
                    end

                    {ADDR_R30_RECV_STATUS, 2'b0}: begin
                        s_axi_lite.rdata <= recv_status;
                        s_axi_lite.rresp <= axi::RESP_OKAY;
                    end

                    {ADDR_R32_RECV_DEQUEUE, 2'b0}: if (q_recv_complete.rvalid) begin
                        s_axi_lite.rresp <= axi::RESP_OKAY;
                        s_axi_lite.rdata <= q_recv_complete.pop();
                    end else begin
                        s_axi_lite.rresp <= axi::RESP_SLVERR;
                    end

                    {ADDR_R33_RECV_ABORT, 2'b0}: if (q_recv_abort.rvalid) begin
                        s_axi_lite.rresp <= axi::RESP_OKAY;
                        s_axi_lite.rdata <= q_recv_abort.pop();
                    end else begin
                        s_axi_lite.rresp <= axi::RESP_SLVERR;
                    end

                    default: begin
                        s_axi_lite.rresp <= axi::RESP_DECERR;
                    end
                endcase
            end

            if (s_axi_lite.rbeat()) begin
                s_axi_lite.rvalid <= 0;
                s_axi_lite.arready <= 1;
            end
        end
    end


    /**************************************************\
    |*                Register: Write                 *|
    \**************************************************/

    reg [9:0] write_addr;

    always @(posedge clock or negedge resetn) begin
        q_packet_outgoing.wvalid <= 0;
        q_send_address.wvalid <= 0;
        q_recv_address.wvalid <= 0;

        if (~resetn) begin
            s_axi_lite.awready <= 1;
            s_axi_lite.wready <= 0;
            s_axi_lite.bvalid <= 0;

        end else begin
            if (s_axi_lite.awbeat()) begin
                write_addr <= s_axi_lite.awaddr;
                s_axi_lite.awready <= 0;
                s_axi_lite.wready <= 1;
            end

            if (s_axi_lite.wbeat()) begin
                s_axi_lite.wready <= 0;
                s_axi_lite.bvalid <= 1;

                case (write_addr)
                    {ADDR_R00_MAIN_CONTROL, 2'b0}: begin
                        s_axi_lite.bresp <= axi::RESP_OKAY;
                        control <= s_axi_lite.get_wdata(control);
                    end

                    {ADDR_R10_PACKET_STATUS, 2'b0}: begin
                        clear_packet_intr(s_axi_lite.wdata, s_axi_lite.wstrb);
                    end

                    {ADDR_R11_PACKET_MESSAGE, 2'b0}: if (q_packet_outgoing.wready && s_axi_lite.wstrb == 4'hF) begin
                        s_axi_lite.bresp <= axi::RESP_OKAY;
                        q_packet_outgoing.push(s_axi_lite.wdata);
                    end else begin
                        s_axi_lite.bresp <= axi::RESP_SLVERR;
                    end

                    {ADDR_R20_SEND_STATUS, 2'b0}: begin
                        clear_send_intr(s_axi_lite.wdata, s_axi_lite.wstrb);
                    end

                    {ADDR_R21_SEND_ENQUEUE, 2'b0}: if (q_send_address.wready && s_axi_lite.wstrb == 4'hF) begin
                        s_axi_lite.bresp <= axi::RESP_OKAY;
                        q_send_address.push(s_axi_lite.wdata);
                    end else begin
                        s_axi_lite.bresp <= axi::RESP_SLVERR;
                    end

                    {ADDR_R30_RECV_STATUS, 2'b0}: begin
                        clear_recv_intr(s_axi_lite.wdata, s_axi_lite.wstrb);
                    end

                    {ADDR_R31_RECV_ENQUEUE, 2'b0}: if (q_recv_address.wready && s_axi_lite.wstrb == 4'hF) begin
                        s_axi_lite.bresp <= axi::RESP_OKAY;
                        q_recv_address.push(s_axi_lite.wdata);
                    end else begin
                        s_axi_lite.bresp <= axi::RESP_SLVERR;
                    end

                    default: begin
                        s_axi_lite.bresp <= axi::RESP_DECERR;
                    end
                endcase
            end

            if (s_axi_lite.bbeat()) begin
                s_axi_lite.bvalid <= 0;
                s_axi_lite.awready <= 1;
            end
        end

        // packet interrupts
        if (~resetn)
            intr_packet_outgoing <= 0;
        else if (m_packet.beat())
            intr_packet_outgoing <= 1;

        if (~resetn)
            intr_packet_incoming <= 0;
        else if (s_packet.beat())
            intr_packet_incoming <= 1;

        // load interrupts
        if (~resetn) begin
            load_intr.ready <= 1;
            intr_send_start <= 0;
            intr_recv_start <= 0;
            intr_send_abort <= 0;
            intr_recv_abort <= 0;

        end else if (load_intr.valid && load_intr.ready) begin
            if (load_intr.send_start) intr_send_start <= 1;
            if (load_intr.recv_start) intr_recv_start <= 1;
            if (load_intr.send_abort) intr_send_abort <= 1;
            if (load_intr.recv_abort) intr_recv_abort <= 1;
        end

        // store interrupts
        if (~resetn) begin
            store_intr.ready <= 1;
            intr_send_done <= 0;
            intr_recv_done <= 0;

        end else if (store_intr.valid && store_intr.ready) begin
            if (store_intr.send_done) intr_send_done <= 1;
            if (store_intr.recv_done) intr_recv_done <= 1;
        end
    end

endmodule