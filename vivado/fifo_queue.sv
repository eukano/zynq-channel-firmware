/*
    Copyright 2019 Ethan Reesor

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// fifo_queue.sv: Queue implementation via Xilinx FIFO Generator

`timescale 1ns / 1ps

module control_queue #(
        DEPTH = 16
    ) (
        input wire clock,
        input wire resetn,

        queue.slave q,
        output wire [$clog2(DEPTH):0] depth
    );

    wire full, empty;
    assign q.wready = ~full;
    assign q.rvalid = ~empty;

    control_fifo fifo (
        .clk(clock),
        .srst(~resetn),
        .full(full),
        .din(q.wdata),
        .wr_en(q.wvalid),
        .empty(empty),
        .dout(q.rdata),
        .rd_en(q.rready),
        .data_count(depth)
    );

endmodule
