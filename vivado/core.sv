/*
    Copyright 2019 Ethan Reesor

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// core.sv: Core module for Vivado projects

`timescale 1ns / 1ps

module core (
        (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLOCK, ASSOCIATED_RESET RESETN, ASSOCIATED_BUSIF S_AXI_LITE:M_AXI_DESC:M_AXI_PACK:S_PACKET:M_PACKET" *)
        (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLOCK CLK" *)
        input wire clock,

        (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RESETN, POLARITY ACTIVE_LOW" *)
        (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RESETN RST" *)
        input wire resetn,

        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE ARVALID" *) input  wire        s_axi_lite_arvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE ARREADY" *) output wire        s_axi_lite_arready,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE ARADDR"  *) input  wire [ 9:0] s_axi_lite_araddr,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE ARPROT"  *) input  wire [ 2:0] s_axi_lite_arprot,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE AWVALID" *) input  wire        s_axi_lite_awvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE AWREADY" *) output wire        s_axi_lite_awready,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE AWADDR"  *) input  wire [ 9:0] s_axi_lite_awaddr,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE AWPROT"  *) input  wire [ 2:0] s_axi_lite_awprot,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RVALID"  *) output wire        s_axi_lite_rvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RREADY"  *) input  wire        s_axi_lite_rready,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA"   *) output wire [31:0] s_axi_lite_rdata,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RRESP"   *) output wire [ 1:0] s_axi_lite_rresp,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE WVALID"  *) input  wire        s_axi_lite_wvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE WREADY"  *) output wire        s_axi_lite_wready,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE WDATA"   *) input  wire [31:0] s_axi_lite_wdata,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE WSTRB"   *) input  wire [ 3:0] s_axi_lite_wstrb,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE BVALID"  *) output wire        s_axi_lite_bvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE BREADY"  *) input  wire        s_axi_lite_bready,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE BRESP"   *) output wire [ 1:0] s_axi_lite_bresp,

        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC ARVALID" *) output wire        m_axi_desc_arvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC ARREADY" *) input  wire        m_axi_desc_arready,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC ARADDR"  *) output wire [31:0] m_axi_desc_araddr,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC ARBURST" *) output wire [ 1:0] m_axi_desc_arburst,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC ARLEN"   *) output wire [ 7:0] m_axi_desc_arlen,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC ARSIZE"  *) output wire [ 2:0] m_axi_desc_arsize,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC ARCACHE" *) output wire [ 3:0] m_axi_desc_arcache,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC ARPROT"  *) output wire [ 2:0] m_axi_desc_arprot,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC AWVALID" *) output wire        m_axi_desc_awvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC AWREADY" *) input  wire        m_axi_desc_awready,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC AWADDR"  *) output wire [31:0] m_axi_desc_awaddr,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC AWBURST" *) output wire [ 1:0] m_axi_desc_awburst,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC AWLEN"   *) output wire [ 7:0] m_axi_desc_awlen,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC AWSIZE"  *) output wire [ 2:0] m_axi_desc_awsize,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC AWCACHE" *) output wire [ 3:0] m_axi_desc_awcache,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC AWPROT"  *) output wire [ 2:0] m_axi_desc_awprot,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC RVALID"  *) input  wire        m_axi_desc_rvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC RREADY"  *) output wire        m_axi_desc_rready,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC RLAST"   *) input  wire        m_axi_desc_rlast,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC RDATA"   *) input  wire [31:0] m_axi_desc_rdata,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC RRESP"   *) input  wire [ 1:0] m_axi_desc_rresp,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC WVALID"  *) output wire        m_axi_desc_wvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC WREADY"  *) input  wire        m_axi_desc_wready,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC WLAST"   *) output wire        m_axi_desc_wlast,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC WDATA"   *) output wire [31:0] m_axi_desc_wdata,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC WSTRB"   *) output wire [ 3:0] m_axi_desc_wstrb,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC BVALID"  *) input  wire        m_axi_desc_bvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC BREADY"  *) output wire        m_axi_desc_bready,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_DESC BRESP"   *) input  wire [ 1:0] m_axi_desc_bresp,

        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK ARVALID" *) output wire        m_axi_pack_arvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK ARREADY" *) input  wire        m_axi_pack_arready,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK ARADDR"  *) output wire [31:0] m_axi_pack_araddr,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK ARBURST" *) output wire [ 1:0] m_axi_pack_arburst,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK ARLEN"   *) output wire [ 7:0] m_axi_pack_arlen,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK ARSIZE"  *) output wire [ 2:0] m_axi_pack_arsize,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK ARCACHE" *) output wire [ 3:0] m_axi_pack_arcache,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK ARPROT"  *) output wire [ 2:0] m_axi_pack_arprot,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK AWVALID" *) output wire        m_axi_pack_awvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK AWREADY" *) input  wire        m_axi_pack_awready,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK AWADDR"  *) output wire [31:0] m_axi_pack_awaddr,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK AWBURST" *) output wire [ 1:0] m_axi_pack_awburst,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK AWLEN"   *) output wire [ 7:0] m_axi_pack_awlen,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK AWSIZE"  *) output wire [ 2:0] m_axi_pack_awsize,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK AWCACHE" *) output wire [ 3:0] m_axi_pack_awcache,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK AWPROT"  *) output wire [ 2:0] m_axi_pack_awprot,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK RVALID"  *) input  wire        m_axi_pack_rvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK RREADY"  *) output wire        m_axi_pack_rready,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK RLAST"   *) input  wire        m_axi_pack_rlast,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK RDATA"   *) input  wire [31:0] m_axi_pack_rdata,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK RRESP"   *) input  wire [ 1:0] m_axi_pack_rresp,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK WVALID"  *) output wire        m_axi_pack_wvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK WREADY"  *) input  wire        m_axi_pack_wready,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK WLAST"   *) output wire        m_axi_pack_wlast,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK WDATA"   *) output wire [31:0] m_axi_pack_wdata,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK WSTRB"   *) output wire [ 3:0] m_axi_pack_wstrb,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK BVALID"  *) input  wire        m_axi_pack_bvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK BREADY"  *) output wire        m_axi_pack_bready,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_AXI_PACK BRESP"   *) input  wire [ 1:0] m_axi_pack_bresp,

        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_PACKET PVALID" *) input  wire        s_packet_pvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_PACKET PREADY" *) output wire        s_packet_pready,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_PACKET PCHAN"  *) input  wire [ 7:0] s_packet_pchan,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_PACKET PLEN"   *) input  wire [15:0] s_packet_plen,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_PACKET PDIR"   *) input  wire        s_packet_pdir,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_PACKET PTYPE"  *) input  wire [ 2:0] s_packet_ptype,

        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_PACKET PVALID" *) output wire        m_packet_pvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_PACKET PREADY" *) input  wire        m_packet_pready,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_PACKET PCHAN"  *) output wire [ 7:0] m_packet_pchan,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_PACKET PLEN"   *) output wire [15:0] m_packet_plen,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_PACKET PDIR"   *) output wire        m_packet_pdir,
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 M_PACKET PTYPE"  *) output wire [ 2:0] m_packet_ptype,

        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 M_STREAM RXVALID" *) output wire        m_stream_rxvalid,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 M_STREAM RXREADY" *) input  wire        m_stream_rxready,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 M_STREAM RXCHAN"  *) output wire [ 7:0] m_stream_rxchan,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 M_STREAM RYVALID" *) input  wire        m_stream_ryvalid,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 M_STREAM RYREADY" *) output wire        m_stream_ryready,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 M_STREAM RYLAST"  *) input  wire        m_stream_rylast,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 M_STREAM RYDATA"  *) input  wire [31:0] m_stream_rydata,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 M_STREAM RYKEEP"  *) input  wire [ 3:0] m_stream_rykeep,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 M_STREAM TXVALID" *) output wire        m_stream_txvalid,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 M_STREAM TXREADY" *) input  wire        m_stream_txready,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 M_STREAM TXCHAN"  *) output wire [ 7:0] m_stream_txchan,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 M_STREAM TXLAST"  *) output wire        m_stream_txlast,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 M_STREAM TXDATA"  *) output wire [31:0] m_stream_txdata,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 M_STREAM TXKEEP"  *) output wire [ 3:0] m_stream_txkeep,

        output wire introut
    );

    axi_lite s_axi_lite();   // Register AXI Slave
    axi_full m_axi_desc();   // Descriptor AXI Master
    axi_full m_axi_pack(); // Packet AXI Master
    packet s_packet();       // Packet Slave
    packet m_packet();       // Packet Master
    stream m_stream();       // Stream Master

    channels channels (.*);

    // bus inputs
    assign s_axi_lite.arvalid = s_axi_lite_arvalid;
    assign s_axi_lite.araddr  = s_axi_lite_araddr;
    assign s_axi_lite.arprot  = s_axi_lite_arprot;
    assign s_axi_lite.awvalid = s_axi_lite_awvalid;
    assign s_axi_lite.awaddr  = s_axi_lite_awaddr;
    assign s_axi_lite.awprot  = s_axi_lite_awprot;
    assign s_axi_lite.rready  = s_axi_lite_rready;
    assign s_axi_lite.wvalid  = s_axi_lite_wvalid;
    assign s_axi_lite.wdata   = s_axi_lite_wdata;
    assign s_axi_lite.wstrb   = s_axi_lite_wstrb;
    assign s_axi_lite.bready  = s_axi_lite_bready;
    assign m_axi_desc.arready = m_axi_desc_arready;
    assign m_axi_desc.awready = m_axi_desc_awready;
    assign m_axi_desc.rvalid  = m_axi_desc_rvalid;
    assign m_axi_desc.rlast   = m_axi_desc_rlast;
    assign m_axi_desc.rdata   = m_axi_desc_rdata;
    assign m_axi_desc.rresp   = m_axi_desc_rresp;
    assign m_axi_desc.wready  = m_axi_desc_wready;
    assign m_axi_desc.bvalid  = m_axi_desc_bvalid;
    assign m_axi_desc.bresp   = m_axi_desc_bresp;
    assign m_axi_pack.arready = m_axi_pack_arready;
    assign m_axi_pack.awready = m_axi_pack_awready;
    assign m_axi_pack.rvalid  = m_axi_pack_rvalid;
    assign m_axi_pack.rlast   = m_axi_pack_rlast;
    assign m_axi_pack.rdata   = m_axi_pack_rdata;
    assign m_axi_pack.rresp   = m_axi_pack_rresp;
    assign m_axi_pack.wready  = m_axi_pack_wready;
    assign m_axi_pack.bvalid  = m_axi_pack_bvalid;
    assign m_axi_pack.bresp   = m_axi_pack_bresp;
    assign s_packet.pvalid    = s_packet_pvalid;
    assign s_packet.pchan     = s_packet_pchan;
    assign s_packet.plen      = s_packet_plen;
    assign s_packet.pdir      = s_packet_pdir;
    assign s_packet.ptype     = s_packet_ptype;
    assign m_packet.pready    = m_packet_pready;
    assign m_stream.rxready   = m_stream_rxready;
    assign m_stream.ryvalid   = m_stream_ryvalid;
    assign m_stream.rylast    = m_stream_rylast;
    assign m_stream.rydata    = m_stream_rydata;
    assign m_stream.rykeep    = m_stream_rykeep;
    assign m_stream.txready   = m_stream_txready;
    assign m_stream.txchan    = m_stream_txchan;
    assign m_stream.txlast    = m_stream_txlast;
    assign m_stream.txdata    = m_stream_txdata;
    assign m_stream.txkeep    = m_stream_txkeep;

    // bus outputs
    assign s_axi_lite_arready = s_axi_lite.arready;
    assign s_axi_lite_awready = s_axi_lite.awready;
    assign s_axi_lite_rvalid  = s_axi_lite.rvalid;
    assign s_axi_lite_rdata   = s_axi_lite.rdata;
    assign s_axi_lite_rresp   = s_axi_lite.rresp;
    assign s_axi_lite_wready  = s_axi_lite.wready;
    assign s_axi_lite_bvalid  = s_axi_lite.bvalid;
    assign s_axi_lite_bresp   = s_axi_lite.bresp;
    assign m_axi_desc_arvalid = m_axi_desc.arvalid;
    assign m_axi_desc_araddr  = m_axi_desc.araddr;
    assign m_axi_desc_arburst = m_axi_desc.arburst;
    assign m_axi_desc_arlen   = m_axi_desc.arlen;
    assign m_axi_desc_arsize  = m_axi_desc.arsize;
    assign m_axi_desc_arcache = m_axi_desc.arcache;
    assign m_axi_desc_arprot  = m_axi_desc.arprot;
    assign m_axi_desc_awvalid = m_axi_desc.awvalid;
    assign m_axi_desc_awaddr  = m_axi_desc.awaddr;
    assign m_axi_desc_awburst = m_axi_desc.awburst;
    assign m_axi_desc_awlen   = m_axi_desc.awlen;
    assign m_axi_desc_awsize  = m_axi_desc.awsize;
    assign m_axi_desc_awcache = m_axi_desc.awcache;
    assign m_axi_desc_awprot  = m_axi_desc.awprot;
    assign m_axi_desc_rready  = m_axi_desc.rready;
    assign m_axi_desc_wvalid  = m_axi_desc.wvalid;
    assign m_axi_desc_wlast   = m_axi_desc.wlast;
    assign m_axi_desc_wdata   = m_axi_desc.wdata;
    assign m_axi_desc_wstrb   = m_axi_desc.wstrb;
    assign m_axi_desc_bready  = m_axi_desc.bready;
    assign m_axi_pack_arvalid = m_axi_pack.arvalid;
    assign m_axi_pack_araddr  = m_axi_pack.araddr;
    assign m_axi_pack_arburst = m_axi_pack.arburst;
    assign m_axi_pack_arlen   = m_axi_pack.arlen;
    assign m_axi_pack_arsize  = m_axi_pack.arsize;
    assign m_axi_pack_arcache = m_axi_pack.arcache;
    assign m_axi_pack_arprot  = m_axi_pack.arprot;
    assign m_axi_pack_awvalid = m_axi_pack.awvalid;
    assign m_axi_pack_awaddr  = m_axi_pack.awaddr;
    assign m_axi_pack_awburst = m_axi_pack.awburst;
    assign m_axi_pack_awlen   = m_axi_pack.awlen;
    assign m_axi_pack_awsize  = m_axi_pack.awsize;
    assign m_axi_pack_awcache = m_axi_pack.awcache;
    assign m_axi_pack_awprot  = m_axi_pack.awprot;
    assign m_axi_pack_rready  = m_axi_pack.rready;
    assign m_axi_pack_wvalid  = m_axi_pack.wvalid;
    assign m_axi_pack_wlast   = m_axi_pack.wlast;
    assign m_axi_pack_wdata   = m_axi_pack.wdata;
    assign m_axi_pack_wstrb   = m_axi_pack.wstrb;
    assign m_axi_pack_bready  = m_axi_pack.bready;
    assign s_packet_pready    = s_packet.pready;
    assign m_packet_pvalid    = m_packet.pvalid;
    assign m_packet_pchan     = m_packet.pchan;
    assign m_packet_plen      = m_packet.plen;
    assign m_packet_pdir      = m_packet.pdir;
    assign m_packet_ptype     = m_packet.ptype;
    assign m_stream_rxvalid   = m_stream.rxvalid;
    assign m_stream_rxchan    = m_stream.rxchan;
    assign m_stream_ryready   = m_stream.ryready;
    assign m_stream_txvalid   = m_stream.txvalid;


endmodule