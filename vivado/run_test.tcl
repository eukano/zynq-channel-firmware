file mkdir sim
cd sim
foreach x $cores {exec xvlog --incr --relax "../zynq-channels.ip_user_files/ip/${x}/sim/${x}.v"}
foreach x $files {exec xvlog --incr --relax --sv "../../${x}.sv"}
exec xelab --incr --relax --debug typical -L fifo_generator_v13_2_4 -L axi_bram_ctrl $top
xsim work.$top -view ../$top.wcfg
cd ..
run