foreach xcix [glob *.xcix] {
    set core [file rootname $xcix]
    lappend cores $core

    set file [get_files $core/$core.xci]
    generate_target all $file
    catch { config_ip_cache -export [get_ips -all $core] }
    export_ip_user_files -of_objects $file -no_script -sync -force -quiet
    create_ip_run $file

    set run [get_runs -quiet "${core}_synth_1"]
    if {$run ne ""} {
        launch_runs -jobs 6 $run
        wait_on_run $run
    }

    export_simulation -of_objects $file -use_ip_compiled_libs -force -quiet \
        -directory ./zynq-channels.ip_user_files/sim_scripts \
        -ip_user_files_dir ./zynq-channels.ip_user_files \
        -ipstatic_source_dir ./zynq-channels.ip_user_files/ipstatic \
        -lib_map_path [list \
            {modelsim=./zynq-channels.cache/compile_simlib/modelsim} \
            {questa=./zynq-channels.cache/compile_simlib/questa} \
            {riviera=./zynq-channels.cache/compile_simlib/riviera} \
            {activehdl=./zynq-channels.cache/compile_simlib/activehdl}]
}