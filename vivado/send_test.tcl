if {[current_sim] ne ""} { close_sim }

set top send_test
set cores {status_fifo descriptor_fifo}
set files {interfaces memory_test send send_test}

cd [file dirname [info script]]
source run_test.tcl