## Copyright 2019 Ethan Reesor
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##       http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.

# create_project.tcl: Initializes Vivado project

create_project "zynq-channels" "." -part xc7z020clg400-3
# set_property board_part "krtkl.com:snickerdoodle_black:part0:1.0" [current_project]

set_property ip_repo_paths [file normalize "./ip"] [current_project]
update_ip_catalog -rebuild

set src [get_filesets sources_1]
set constrs [get_filesets constrs_1]
set sim [get_filesets sim_1]

foreach x [glob "{.,..}/*.{v,sv}"] {
    if [string match "*_test" [file rootname $x]] {
        add_files -fileset $sim $x
    } else {
        add_files -fileset $src $x
    }
}

add_files -fileset $src "./ip/component.xml"
add_files -fileset $constrs [glob "./*.xdc"]
add_files -fileset $sim [glob "./*.wcfg"]

set_property file_type "IP-XACT" [get_files "./ip/component.xml"]
set_property file_type SystemVerilog [get_files [glob "./*.sv" "../*.sv"]]
set_property file_type XDC [get_files [glob "./*.xdc"]]

set_property used_in_simulation false [get_files [list "./core.sv" "./core.v"]]

set_property top zynq_channels $src
set_property top channels_test $sim

# create AXI BRAM for testing
create_ip -name axi_bram_ctrl -vendor xilinx.com -library ip -version 4.1 -module_name axi_bram_test
set_property -object [get_ips axi_bram_test] CONFIG.BMG_INSTANCE INTERNAL
set_property used_in_synthesis false [get_files [get_property IP_FILE [get_ips axi_bram_test]]]
set_property used_in_implementation false [get_files [get_property IP_FILE [get_ips axi_bram_test]]]

# create FIFO for control register
create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.2 -module_name control_fifo
set width 32
set depth 16
set count [expr int(ceil(log($depth+2)/log(2)))]
set_property -object [get_ips control_fifo] CONFIG.Fifo_Implementation Common_Clock_Distributed_RAM
set_property -object [get_ips control_fifo] CONFIG.Performance_Options First_Word_Fall_Through
set_property -object [get_ips control_fifo] CONFIG.Input_Data_Width $width
set_property -object [get_ips control_fifo] CONFIG.Input_Depth $depth
set_property -object [get_ips control_fifo] CONFIG.Output_Data_Width $width
set_property -object [get_ips control_fifo] CONFIG.Output_Depth $depth
set_property -object [get_ips control_fifo] CONFIG.Use_Extra_Logic true
set_property -object [get_ips control_fifo] CONFIG.Data_Count true
set_property -object [get_ips control_fifo] CONFIG.Data_Count_Width $count
set_property -object [get_ips control_fifo] CONFIG.Write_Data_Count_Width $count
set_property -object [get_ips control_fifo] CONFIG.Read_Data_Count_Width $count
set_property -object [get_ips control_fifo] CONFIG.Full_Threshold_Assert_Value [expr $depth-1]
set_property -object [get_ips control_fifo] CONFIG.Full_Threshold_Negate_Value [expr $depth-2]
set_property -object [get_ips control_fifo] CONFIG.Empty_Threshold_Assert_Value 4
set_property -object [get_ips control_fifo] CONFIG.Empty_Threshold_Negate_Value 5

# generate HDL wrapper for IP
foreach ip [get_ips] {generate_target {instantiation_template} [get_files [get_property IP_FILE $ip]]}

# allow IP to be embedded in a block diagram
set_property generate_synth_checkpoint 0 [get_files *.xci]