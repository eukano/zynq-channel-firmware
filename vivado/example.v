/*
    Copyright 2019 Ethan Reesor

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// example.v: An example module using Zynq Channels

`timescale 1ns / 1ps

module example (
        (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLOCK, ASSOCIATED_RESET RESETN, ASSOCIATED_BUSIF S_PACKET:M_PACKET:S_STREAM" *)
        (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLOCK CLK" *)
        input wire clock,

        (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RESETN, POLARITY ACTIVE_LOW" *)
        (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RESETN RST" *)
        input wire resetn,

        (* X_INTERFACE_INFO = "eukano.com:channels:packet:1.0 S_PACKET PVALID"  *) input wire        s_packet_pvalid,
        (* X_INTERFACE_INFO = "eukano.com:channels:packet:1.0 S_PACKET PREADY"  *) output reg        s_packet_pready,
        (* X_INTERFACE_INFO = "eukano.com:channels:packet:1.0 S_PACKET PCHAN"   *) input wire [ 7:0] s_packet_pchan,
        (* X_INTERFACE_INFO = "eukano.com:channels:packet:1.0 S_PACKET PLEN"    *) input wire [15:0] s_packet_plen,
        (* X_INTERFACE_INFO = "eukano.com:channels:packet:1.0 S_PACKET PDIR"    *) input wire        s_packet_pdir,
        (* X_INTERFACE_INFO = "eukano.com:channels:packet:1.0 S_PACKET PTYPE"   *) input wire [ 2:0] s_packet_ptype,

        (* X_INTERFACE_INFO = "eukano.com:channels:packet:1.0 M_PACKET PVALID"  *) output reg        m_packet_pvalid,
        (* X_INTERFACE_INFO = "eukano.com:channels:packet:1.0 M_PACKET PREADY"  *) input wire        m_packet_pready,
        (* X_INTERFACE_INFO = "eukano.com:channels:packet:1.0 M_PACKET PCHAN"   *) output reg [ 7:0] m_packet_pchan,
        (* X_INTERFACE_INFO = "eukano.com:channels:packet:1.0 M_PACKET PLEN"    *) output reg [15:0] m_packet_plen,
        (* X_INTERFACE_INFO = "eukano.com:channels:packet:1.0 M_PACKET PDIR"    *) output reg        m_packet_pdir,
        (* X_INTERFACE_INFO = "eukano.com:channels:packet:1.0 M_PACKET PTYPE"   *) output reg [ 2:0] m_packet_ptype,

        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 S_STREAM RXVALID" *) input wire        s_stream_rxvalid,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 S_STREAM RXREADY" *) output reg        s_stream_rxready,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 S_STREAM RXCHAN"  *) input wire [ 7:0] s_stream_rxchan,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 S_STREAM RYVALID" *) output reg        s_stream_ryvalid,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 S_STREAM RYREADY" *) input wire        s_stream_ryready,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 S_STREAM RYLAST"  *) output reg        s_stream_rylast,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 S_STREAM RYDATA"  *) output reg [31:0] s_stream_rydata,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 S_STREAM RYKEEP"  *) output reg [ 3:0] s_stream_rykeep,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 S_STREAM TXVALID" *) input wire        s_stream_txvalid,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 S_STREAM TXREADY" *) output reg        s_stream_txready,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 S_STREAM TXCHAN"  *) input wire [ 7:0] s_stream_txchan,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 S_STREAM TXLAST"  *) input wire        s_stream_txlast,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 S_STREAM TXDATA"  *) input wire [31:0] s_stream_txdata,
        (* X_INTERFACE_INFO = "eukano.com:channels:stream:1.0 S_STREAM TXKEEP"  *) input wire [ 3:0] s_stream_txkeep
    );

    /*
     * 1. -> N available for send
     * 2. <- 1 available for receive
     * 3. -> 1 accepted for receive
     * 4. <- N accepted for send (except PLEN is don't care when accepting)
     */

    always @(posedge clock or negedge resetn) begin
        if (~resetn) begin
            s_packet_pready <= 1;
            m_packet_pvalid <= 0;

        end else begin
            if (s_packet_pvalid && s_packet_pready) begin
                s_packet_pready <= 0;
                m_packet_pvalid <= 1;
                m_packet_pchan <= s_packet_pchan;
                m_packet_plen <= s_packet_pdir ? 4 : 0;
                m_packet_pdir <= s_packet_pdir ? 0 : 1;
                m_packet_ptype <= s_packet_ptype;
            end

            if (m_packet_pvalid && m_packet_pready) begin
                m_packet_pvalid <= 0;
                s_packet_pready <= 1;
            end
        end
    end

    reg [31:0] sum;

    always @(posedge clock or negedge resetn) begin
        if (~resetn) begin
            s_stream_txready <= 1;
            s_stream_rxready <= 1;
            s_stream_ryvalid <= 0;

        end else begin
            if (s_stream_txvalid && s_stream_txready) begin
                sum <= sum + s_stream_txdata; // ignore keep, channel
            end

            if (s_stream_ryvalid && s_stream_ryready) begin
                s_stream_ryvalid <= 0;
                sum <= 0;
            end

            if (s_stream_rxvalid && s_stream_rxready) begin
                s_stream_ryvalid <= 1;
                s_stream_rylast <= 1;
                s_stream_rydata <= sum;
                s_stream_rykeep <= 'hF;
            end
        end
    end

endmodule