# set board to snickerdoodle
set_property board_part "krtkl.com:snickerdoodle_black:part0:1.0" [current_project]

# create block diagram
create_bd_design system
set bd [get_property FILE_NAME [current_bd_design]]
set wrapper [make_wrapper -files [get_files $bd] -top]
add_files -norecurse $wrapper
set_property used_in_simulation false [get_files [list $bd $wrapper]]


# make top
set_property top system_wrapper [current_fileset]

# add core
create_bd_cell -type module -reference zynq_channels zynq_channels_0

# add zynq processing system, apply board preset
create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0
apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" apply_board_preset "1" Master "Disable" Slave "Disable" }  [get_bd_cells processing_system7_0]

# add reset subsystem
create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 proc_sys_reset_0

# connect clock and reset
connect_bd_net [get_bd_pins processing_system7_0/FCLK_CLK0] [get_bd_pins proc_sys_reset_0/slowest_sync_clk]
connect_bd_net [get_bd_pins processing_system7_0/FCLK_RESET0_N] [get_bd_pins proc_sys_reset_0/ext_reset_in]

# disable ACP, enable HP0
set_property -dict [list CONFIG.PCW_USE_S_AXI_ACP {0} CONFIG.PCW_USE_S_AXI_HP0 {1} CONFIG.PCW_QSPI_GRP_SINGLE_SS_ENABLE {1}] [get_bd_cells processing_system7_0]

# connect AXI-Lite to PS
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/processing_system7_0/M_AXI_GP0} Slave {/zynq_channels_0/S_AXI_LITE} intc_ip {New AXI Interconnect} master_apm {0}}  [get_bd_intf_pins zynq_channels_0/S_AXI_LITE]

# connect AXI to DDR controller
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {/processing_system7_0/FCLK_CLK0 (49 MHz)} Clk_slave {Auto} Clk_xbar {Auto} Master {/zynq_channels_0/M_AXI_DESC} Slave {/processing_system7_0/S_AXI_HP0} intc_ip {Auto} master_apm {0}}  [get_bd_intf_pins processing_system7_0/S_AXI_HP0]
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {/processing_system7_0/FCLK_CLK0 (49 MHz)} Clk_slave {/processing_system7_0/FCLK_CLK0 (49 MHz)} Clk_xbar {/processing_system7_0/FCLK_CLK0 (49 MHz)} Master {/zynq_channels_0/M_AXI_PACK} Slave {/processing_system7_0/S_AXI_HP0} intc_ip {/axi_smc} master_apm {0}}  [get_bd_intf_pins zynq_channels_0/M_AXI_PACK]

# connect interrupt
connect_bd_net [get_bd_pins zynq_channels_0/introut] [get_bd_pins processing_system7_0/IRQ_F2P]

# finalize
validate_bd_design
regenerate_bd_layout
save_bd_design
update_compile_order -fileset sources_1