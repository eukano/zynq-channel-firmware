/*
    Copyright 2019 Ethan Reesor

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// load.sv: Reads packet descriptors from memory

`timescale 1ns / 1ps

interface load_intr();
    logic valid, ready, send_start, recv_start, send_abort, recv_abort;
    modport slave(output valid, send_start, recv_start, send_abort, recv_abort, input ready);
endinterface

module load (
        input wire clock,
        input wire resetn,

        load_intr.slave intr,

        axi_full.reader memory,            // AXI Master
        queue.reader    q_send_address,    // Send Address Queue
        queue.reader    q_recv_address,    // Receive Address Queue
        queue.writer    q_send_descriptor, // Send Descriptor Queue
        queue.writer    q_recv_descriptor, // Receive Descriptor Queue
        queue.writer    q_send_abort,      // Send Abort Queue
        queue.writer    q_recv_abort       // Receive Abort Queue
    );


    enum {IDLE, ADDR, DATA, PUSH, DRAIN, ERROR} state;
    enum {SEND, RECV} mode;
    reg send_valid, recv_valid;
    reg [31:0] send_addr, recv_addr;
    reg [$clog2(driver::DESCRIPTOR_WORDS)-1:0] index;
    reg [driver::DESCRIPTOR_BITS-1:0] value;

    wire [31:0] init_address = q_send_address.rvalid ? q_send_address.rdata : q_recv_address.rdata;

    always @(posedge clock or negedge resetn) begin
        q_send_address.rready <= 0;
        q_recv_address.rready <= 0;
        q_send_descriptor.wvalid <= 0;
        q_recv_descriptor.wvalid <= 0;

        if (~resetn) begin
            intr.valid <= 0;
            intr.send_start <= 0;
            intr.recv_start <= 0;
            intr.send_abort <= 0;
            intr.recv_abort <= 0;

        end else if (intr.valid && intr.ready) begin
            intr.send_start <= 0;
            intr.recv_start <= 0;
            intr.send_abort <= 0;
            intr.recv_abort <= 0;
        end

        if (~resetn) begin
            state <= IDLE;
            mode <= SEND;
            memory.arvalid <= 0;
            memory.rready <= 0;

        end else case (state)
            IDLE: if (q_send_address.rvalid && q_send_abort.wready || q_recv_address.rvalid && q_recv_abort.wready) begin
                state <= ADDR;
                mode <= q_send_address.rvalid ? SEND : RECV;
                send_valid <= q_send_address.rvalid;
                recv_valid <= q_recv_address.rvalid;

                send_addr = q_send_address.pop();
                recv_addr = q_recv_address.pop();

                intr.valid <= 1;
                if (q_send_address.rvalid)
                    intr.send_start <= 1;
                else
                    intr.recv_start <= 1;

                memory.read_address(1, init_address, axi::BURST_INCR, axi::BURST_32_BIT, driver::DESCRIPTOR_WORDS, axi::NORMAL_NOCACHE, axi::UNPRIV_SECURE_DATA);
            end

            ADDR: if (memory.arready) begin
                state <= DATA;
                index <= 0;
                if (mode == SEND)
                    send_valid <= 0;
                else
                    recv_valid <= 0;

                memory.arvalid <= 0;
                memory.rready <= 1;
            end

            DATA: if (memory.rvalid) begin
                if (memory.rresp != axi::RESP_OKAY || memory.rlast ^ index == driver::DESCRIPTOR_WORDS - 1) begin
                    intr.valid <= 1;
                    if (mode == SEND) begin
                        intr.send_abort <= 1;
                        q_send_abort.push(send_addr);
                    end else begin
                        intr.recv_abort <= 1;
                        q_recv_abort.push(recv_addr);
                    end

                    if (memory.rlast) begin
                        state <= ERROR;
                        memory.rready <= 0;
                    end else
                        state <= DRAIN;

                end else begin
                    value <= {value[driver::DESCRIPTOR_BITS-33:0], memory.rdata};
                    if (index < driver::DESCRIPTOR_WORDS - 1)
                        index <= index + 1;
                    else begin
                        state <= PUSH;
                        memory.rready <= 0;
                    end
                end
            end

            PUSH: case (mode)
                SEND: if (q_send_descriptor.wready) begin
                    q_send_descriptor.push({send_addr, value});

                    if (recv_valid) begin
                        state <= ADDR;
                        mode <= RECV;
                        intr.valid <= 1;
                        intr.recv_start <= 1;
                        memory.araddr <= recv_addr;
                        memory.arvalid <= 1;
                        // no cache, can modify, can buffer; unprivileged secure data
                        memory.read_address(1, recv_addr, axi::BURST_INCR, axi::BURST_32_BIT, driver::DESCRIPTOR_WORDS, axi::NORMAL_NOCACHE, axi::UNPRIV_SECURE_DATA);


                    end else begin
                        state <= IDLE;
                    end
                end

                RECV: if (q_recv_descriptor.wready) begin
                    q_recv_descriptor.push({recv_addr, value});
                    state <= IDLE;
                end
            endcase

            DRAIN: if (memory.rvalid && memory.rlast) begin
                state <= ERROR;
                memory.rready <= 0;
            end

            ERROR: begin
                state <= IDLE;
            end
        endcase
    end

endmodule