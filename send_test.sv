/*
    Copyright 2019 Ethan Reesor

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// send_test.sv: Tests for the send module

`timescale 1ns / 1ps

module send_test();
    import test::*;

    reg clock, resetn;
    enum {RESET, UNALIGNED, PAGES, DONE} test;
    reg [31:0] index, address, length;

    initial clock = 1;
    always #1 clock = ~clock;

    initial begin
        test = RESET;
        memory.arvalid <= 0;
        memory.rready <= 0;
        q_send_descriptor.wvalid <= 0;
        q_send_status.rready <= 0;
        m_stream.rxready <= 0;
        m_stream.ryvalid <= 0;

        resetn = 0;
        @(posedge clock);
        resetn = 1;

        test_unaligned();
        test_pages();
        // test_short();
        // test_long();
        complete();
    end

    localparam MEM_OFFSET = aximem.mem.OFFSET;
    localparam DESC_ADDR = 'h120;
    localparam PACK_ADDR = 'h320;
    localparam PACK_CHAN = 'h9;
    localparam PACK_ID = 'hA;
    localparam PACK_LEN = 'h20;
    localparam PACK_STATUS = 'h5A5A5A00;

    localparam integer PACK_DATA [0:7] = '{
        'h2b1ee54e,
        'h802a8c1a,
        'h92673584,
        'hb4fd462c,
        'hcf71386a,
        'h8633ee0c,
        'hcdc278ff,
        'h098f30d6
    };

    task test_unaligned();
        test = UNALIGNED;
        $display("Testing unaligned packet");

        aximem.mem.M[PACK_ADDR/4+0] = PACK_DATA[0];
        aximem.mem.M[PACK_ADDR/4+1] = PACK_DATA[1];
        aximem.mem.M[PACK_ADDR/4+2] = PACK_DATA[2];
        aximem.mem.M[PACK_ADDR/4+3] = PACK_DATA[3];
        aximem.mem.M[PACK_ADDR/4+4] = PACK_DATA[4];
        aximem.mem.M[PACK_ADDR/4+5] = PACK_DATA[5];
        aximem.mem.M[PACK_ADDR/4+6] = PACK_DATA[6];
        aximem.mem.M[PACK_ADDR/4+7] = PACK_DATA[7];

        m_stream.txready <= 1;
        q_send_descriptor.push({
            DESC_ADDR + MEM_OFFSET,
            driver::descriptor_bits('{
                address: PACK_ADDR + MEM_OFFSET + 1,
                channel: PACK_CHAN,
                id: PACK_ID,
                length: PACK_LEN - 2,
                status: PACK_STATUS
            })
        });

        @(posedge clock);
        q_send_descriptor.wvalid <= 0;

        for (int i = 0; i < 8; i++) begin
            @(posedge clock);
            while (~m_stream.txbeat())
                @(posedge clock);

            assert_equal(m_stream.txlast, i == 7, "TXLAST asserted");
            assert_equal(m_stream.txkeep, {i > 0, 2'b11, i < 7}, "TXKEEP incorrect");
            assert_equal(m_stream.txchan, PACK_CHAN, "TXCHAN incorrect");
            assert_data(m_stream.txdata, PACK_DATA[i], m_stream.txkeep, "TXDATA incorrect");
        end

        m_stream.txready <= 0;

        while (~q_send_status.rvalid)
            @(posedge clock);

        assert_equal(q_send_status.rdata[63:32], DESC_ADDR + MEM_OFFSET, "Descriptor address invalid");
        assert_equal(q_send_status.rdata[31: 0], PACK_STATUS | driver::TRANSFER_COMPLETE, "Packet status invalid");

        q_send_status.pop();
        @(posedge clock);
        q_send_status.rready <= 0;
    endtask

    task test_pages();
        test = PAGES;
        $display("Testing multi-page packet");

        address = 1 << (send.BOUNDARY - 1);
        length = 1 << (send.BOUNDARY + 1);
        index = 'h01020304;
        for (integer i = 0; i < length/4; i++) begin
            aximem.mem.M[address/4 + i] = index;
            index[31:8] = index[23:0];
            index[7:0] = index[15:8] + 1;
        end

        m_stream.txready <= 1;
        q_send_descriptor.push({
            DESC_ADDR + MEM_OFFSET,
            driver::descriptor_bits('{
                address: PACK_ADDR + MEM_OFFSET + 1,
                channel: PACK_CHAN,
                id: PACK_ID,
                length: length - 2,
                status: PACK_STATUS
            })
        });

        @(posedge clock);
        q_send_descriptor.wvalid <= 0;

        while (~m_stream.txbeat() || ~m_stream.txlast)
            @(posedge clock);

        m_stream.txready <= 0;

        while (~q_send_status.rvalid)
            @(posedge clock);

        assert_equal(q_send_status.rdata[63:32], DESC_ADDR + MEM_OFFSET, "Descriptor address invalid");
        assert_equal(q_send_status.rdata[31: 0], PACK_STATUS | driver::TRANSFER_COMPLETE, "Packet status invalid");

        q_send_status.pop();
        @(posedge clock);
        q_send_status.rready <= 0;
    endtask

    task complete();
        test = DONE;
        $display("Tests complete");
        @(posedge clock);
        $finish();
    endtask

    axi_full memory();
    queue #(.WIDTH(128)) q_send_descriptor();
    queue #(.WIDTH(64)) q_send_status();
    stream m_stream();

    axi_memory aximem (.*, .A(memory));
    flat_queue #(.WIDTH(128)) q_send_descriptor_impl(.*, .q(q_send_descriptor));
    flat_queue #(.WIDTH(64)) q_send_status_impl(.*, .q(q_send_status));

    send #(.BOUNDARY(axi::BURST_32_BIT + 4)) send(.*);

endmodule