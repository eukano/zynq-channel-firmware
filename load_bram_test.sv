/*
    Copyright 2019 Ethan Reesor

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// load_bram_test.sv: Tests for the load module using the AXI BRAM controller

`timescale 1ns / 1ps

module load_bram_test();
    reg clock, resetn;

    initial clock = 1;
    always #1 clock = ~clock;

    initial resetn = 0;
    always #10 resetn = 1;

    localparam DESC_ADDR = 'h120;
    localparam PACK_TAG = 'hDEADBEEF;
    localparam PACK_STATUS = 'h5A5A5A5A;
    localparam PACK_ADDR = 'h320;
    localparam PACK_LEN = 'h40;

    axi_full memory();
    queue #(.WIDTH(32)) send_address(), recv_address();
    queue #(.WIDTH(128)) send_descriptor(), recv_descriptor();
    queue #(.WIDTH(32)) send_abort(), recv_abort();

    flat_queue #(.WIDTH(32)) send_address_impl(.*, .q(send_address));
    flat_queue #(.WIDTH(32)) recv_address_impl(.*, .q(recv_address));
    flat_queue #(.WIDTH(128)) send_descriptor_impl(.*, .q(send_descriptor));
    flat_queue #(.WIDTH(128)) recv_descriptor_impl(.*, .q(recv_descriptor));
    flat_queue #(.WIDTH(32)) send_abort_impl(.*, .q(send_abort));
    flat_queue #(.WIDTH(32)) recv_abort_impl(.*, .q(recv_abort));

    load load (.*);

    always @* begin
        send_descriptor.rready <= 0;
        recv_descriptor.rready <= 0;
    end

    enum {
        WRITE_TAG,
        WRITE_STATUS,
        WRITE_ADDR,
        WRITE_LEN,
        PUSH_ADDR,
        DONE
    } state;

    always @(posedge clock or negedge resetn) begin
        if (~resetn) begin
            state <= WRITE_TAG;
            memory.awvalid <= 0;
            memory.wvalid <= 0;
            memory.bready <= 0;
            send_address.wvalid <= 0;

        end else if (memory.awvalid || memory.wvalid || memory.bready || send_address.wvalid) begin
            if (memory.awvalid && memory.awready) begin
                memory.awvalid <= 0;
                memory.wvalid <= 1;
            end else if (memory.wvalid && memory.wready) begin
                memory.wvalid <= 0;
                memory.bready <= 1;
            end else if (memory.bvalid && memory.bready)
                memory.bready <= 0;

            if (send_address.wvalid)
                send_address.wvalid <= 0;

        end else case (state)
            WRITE_TAG: begin
                memory.write_address(1, DESC_ADDR, axi::BURST_INCR, axi::BURST_32_BIT, 1, axi::NORMAL_NOCACHE, axi::UNPRIV_SECURE_DATA);
                memory.write_data(0, PACK_TAG, 'b1111, 1);
                state <= WRITE_STATUS;
            end

            WRITE_STATUS: begin
                memory.write_address(1, DESC_ADDR + 4, axi::BURST_INCR, axi::BURST_32_BIT, 1, axi::NORMAL_NOCACHE, axi::UNPRIV_SECURE_DATA);
                memory.write_data(0, PACK_STATUS, 'b1111, 1);
                state <= WRITE_ADDR;
            end

            WRITE_ADDR: begin
                memory.write_address(1, DESC_ADDR + 8, axi::BURST_INCR, axi::BURST_32_BIT, 1, axi::NORMAL_NOCACHE, axi::UNPRIV_SECURE_DATA);
                memory.write_data(0, PACK_ADDR, 'b1111, 1);
                state <= WRITE_LEN;
            end

            WRITE_LEN: begin
                memory.write_address(1, DESC_ADDR + 12, axi::BURST_INCR, axi::BURST_32_BIT, 1, axi::NORMAL_NOCACHE, axi::UNPRIV_SECURE_DATA);
                memory.write_data(0, PACK_LEN, 'b1111, 1);
                state <= PUSH_ADDR;
            end

            PUSH_ADDR: begin
                send_address.push(DESC_ADDR);
                state <= DONE;
            end
        endcase
    end

    axi_bram_test axi_bram_test (
        .s_axi_aclk(clock),
        .s_axi_aresetn(resetn),
        .s_axi_awaddr(memory.awaddr),
        .s_axi_awlen(memory.awlen),
        .s_axi_awsize(memory.awsize),
        .s_axi_awburst(memory.awburst),
        .s_axi_awlock('b00),
        .s_axi_awcache(memory.awcache),
        .s_axi_awprot(memory.awprot),
        .s_axi_awvalid(memory.awvalid),
        .s_axi_awready(memory.awready),
        .s_axi_wdata(memory.wdata),
        .s_axi_wstrb(memory.wstrb),
        .s_axi_wlast(memory.wlast),
        .s_axi_wvalid(memory.wvalid),
        .s_axi_wready(memory.wready),
        .s_axi_bresp(memory.bresp),
        .s_axi_bvalid(memory.bvalid),
        .s_axi_bready(memory.bready),
        .s_axi_araddr(memory.araddr),
        .s_axi_arlen(memory.arlen),
        .s_axi_arsize(memory.arsize),
        .s_axi_arburst(memory.arburst),
        .s_axi_arlock('b00),
        .s_axi_arcache(memory.arcache),
        .s_axi_arprot(memory.arprot),
        .s_axi_arvalid(memory.arvalid),
        .s_axi_arready(memory.arready),
        .s_axi_rdata(memory.rdata),
        .s_axi_rresp(memory.rresp),
        .s_axi_rlast(memory.rlast),
        .s_axi_rvalid(memory.rvalid),
        .s_axi_rready(memory.rready)
    );

endmodule
