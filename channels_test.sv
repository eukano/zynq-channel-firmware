/*
    Copyright 2019 Ethan Reesor

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// channels_test.sv: Tests for the channels module

`timescale 1ns / 1ps

module channels_test();
    import test::*;

    reg clock, resetn;
    enum {RESET, SEND, RECV, PACKET, DONE, FAILED} test;
    reg [31:0] value, len;
    wire introut;

    initial clock = 1;
    always #1 clock = ~clock;

    initial begin
        test = RESET;
        axi_lite.arvalid = 0;
        axi_lite.awvalid = 0;
        axi_lite.rready = 0;
        axi_lite.wvalid = 0;
        axi_lite.bready = 0;
        s_packet.pvalid = 0;
        m_packet.pready = 0;
        stream.txready = 1;
        stream.rxready = 1;
        stream.ryvalid = 0;

        resetn = 0;
        @(posedge clock);
        resetn = 1;

        test_send();
        test_recv();
        test_packet();
        complete();
    end

    localparam MEM_OFFSET = aximem.mem.OFFSET;
    localparam D1_ADDR = 'h120;
    localparam P1_ADDR = 'h320;
    localparam P1_CHAN = 'h9;
    localparam P1_ID = 'hA;
    localparam P1_LEN = 'h20;
    localparam P1_STATUS = 'h5A5A5A00;

    task test_send();
        test = SEND;
        $display("Testing packet send");

        write_descriptor(D1_ADDR, '{address: P1_ADDR + MEM_OFFSET, channel: P1_CHAN, id: P1_ID, length: P1_LEN, status: P1_STATUS});
        write_register(channels.ADDR_R21_SEND_ENQUEUE, D1_ADDR + MEM_OFFSET);

        @(posedge channels.introut);
        read_register(channels.ADDR_R20_SEND_STATUS, value);

        if (value & (1 << channels.POS_SEND_INTR_START) == 0) begin
            $display("Packet send was not started");
            $stop;
        end

        write_register(channels.ADDR_R20_SEND_STATUS, 1 << channels.POS_SEND_INTR_START);

        @(posedge channels.introut);
        read_register(channels.ADDR_R20_SEND_STATUS, value);

        if (value & (1 << channels.POS_SEND_INTR_DONE) == 0) begin
            $display("Packet send was not completed");
            $stop;
        end

        write_register(channels.ADDR_R20_SEND_STATUS, 1 << channels.POS_SEND_INTR_DONE);

        // let the fifo propagate
        @(posedge clock);
        @(posedge clock);

        read_register(channels.ADDR_R22_SEND_DEQUEUE, value);
        assert_equal(value, D1_ADDR + MEM_OFFSET, "Expected descriptor address");
    endtask

    task test_recv();
        test = RECV;
        $display("Testing packet receive");

        write_descriptor(D1_ADDR, '{address: P1_ADDR + MEM_OFFSET, channel: P1_CHAN, id: P1_ID, length: P1_LEN, status: P1_STATUS});
        write_register(channels.ADDR_R31_RECV_ENQUEUE, D1_ADDR + MEM_OFFSET);

        @(posedge channels.introut);
        read_register(channels.ADDR_R30_RECV_STATUS, value);

        if (value & (1 << channels.POS_SEND_INTR_START) == 0) begin
            $display("Packet receive was not started");
            $stop;
        end

        write_register(channels.ADDR_R30_RECV_STATUS, 1 << channels.POS_RECV_INTR_START);

        len = P1_LEN;
        while (len > 0) begin
            len = len - 4;
            stream.rysend(1, 'hDEADBEEF, 'hF, len == 0);
            @(posedge clock);
            while (~stream.rybeat()) @(posedge clock);
            stream.ryvalid <= 0;
        end

        @(posedge channels.introut);
        read_register(channels.ADDR_R30_RECV_STATUS, value);

        if (value & (1 << channels.POS_RECV_INTR_DONE) == 0) begin
            $display("Packet receive was not completed");
            $stop;
        end

        write_register(channels.ADDR_R30_RECV_STATUS, 1 << channels.POS_RECV_INTR_DONE);

        // let the fifo propagate
        @(posedge clock);
        @(posedge clock);

        read_register(channels.ADDR_R32_RECV_DEQUEUE, value);
        assert_equal(value, D1_ADDR + MEM_OFFSET, "Expected descriptor address");
    endtask

    task test_packet();
        automatic driver::message msg;

        test = PACKET;

        $display("Testing packet master");
        write_register(channels.ADDR_R11_PACKET_MESSAGE, driver::message_bits('{channel: 'h3, unused: 'h0, direction: channel::SEND, typ: channel::REJECTED, length: 'h1234}));

        m_packet.pready = 1;

        @(posedge clock);
        while (~m_packet.beat())
            @(posedge clock);

        assert_equal(m_packet.pchan, 'h3, "Invalid packet channel");
        assert_equal(m_packet.pdir, channel::SEND, "Invalid packet direction");
        assert_equal(m_packet.ptype, channel::REJECTED, "Invalid packet type");
        assert_equal(m_packet.plen, 'h1234, "Invalid packet length");

        @(posedge channels.introut);
        m_packet.pready = 0;
        read_register(channels.ADDR_R10_PACKET_STATUS, value);


        if (value & (1 << channels.POS_PACKET_INTR_OUTGOING) == 0) begin
            $display("Packet master did not send outgoing message");
            $stop;
        end

        write_register(channels.ADDR_R10_PACKET_STATUS, 1 << channels.POS_PACKET_INTR_OUTGOING);

        $display("Testing packet slave");
        s_packet.pvalid = 1;
        s_packet.pchan = 'h5;
        s_packet.pdir = channel::RECEIVE;
        s_packet.ptype = channel::COMPLETED;
        s_packet.plen = 'hABCD;

        @(posedge clock);
        while (~s_packet.beat())
            @(posedge clock);

        s_packet.pvalid = 0;

        if (~channels.introut)
            @(posedge channels.introut);
        read_register(channels.ADDR_R10_PACKET_STATUS, value);


        if (value & (1 << channels.POS_PACKET_INTR_INCOMING) == 0) begin
            $display("Packet master did not send incoming message");
            $stop;
        end

        write_register(channels.ADDR_R10_PACKET_STATUS, 1 << channels.POS_PACKET_INTR_INCOMING);

        read_register(channels.ADDR_R11_PACKET_MESSAGE, value);
        msg = driver::as_message(value);
        assert_equal(msg.channel, 'h5, "Invalid packet channel");
        assert_equal(msg.direction, channel::RECEIVE, "Invalid packet direction");
        assert_equal(msg.typ, channel::COMPLETED, "Invalid packet type");
        assert_equal(msg.length, 'hABCD, "Invalid packet length");
    endtask

    task complete();
        test = DONE;
        $display("Tests complete");
        @(posedge clock);
        $finish();
    endtask

    /* ************************************************************ */
    /* ************************************************************ */
    /* ************************************************************ */

    axi_lite axi_lite();
    axi_full axi_desc();
    axi_full axi_pack();
    packet s_packet();
    packet m_packet();
    stream stream();

    dual_axi_memory aximem(.*, .A(axi_desc), .B(axi_pack));

    channels channels(.*, .s_axi_lite(axi_lite), .m_axi_desc(axi_desc), .m_axi_pack(axi_pack), .m_stream(stream));

    task write_descriptor(input [31:0] addr, input driver::descriptor desc);
        {
            aximem.mem.M[D1_ADDR/4+0],
            aximem.mem.M[D1_ADDR/4+1],
            aximem.mem.M[D1_ADDR/4+2]
        } = driver::descriptor_bits(desc);
    endtask

    task read_register(input [7:0] addr, output [31:0] value);
        axi_lite.read_address(1, {addr, 2'b0}, axi::UNPRIV_SECURE_DATA);
        while (~axi_lite.arbeat()) @(posedge clock);
        axi_lite.arvalid <= 0;

        axi_lite.rready <= 1;
        while (~axi_lite.rbeat()) @(posedge clock);
        axi_lite.rready <= 0;
        value = axi_lite.rdata;
        assert_equal(axi_lite.rresp, axi::RESP_OKAY, "Failed to read register");
    endtask

    task write_register(input [7:0] addr, input [31:0] value);
        axi_lite.write_address(1, {addr, 2'b0}, axi::UNPRIV_SECURE_DATA);
        while (~axi_lite.awbeat()) @(posedge clock);
        axi_lite.awvalid <= 0;

        axi_lite.write_data(1, value, 4'hF);
        while (~axi_lite.wbeat()) @(posedge clock);
        axi_lite.wvalid <= 0;

        axi_lite.bready <= 1;
        while (~axi_lite.bbeat()) @(posedge clock);
        axi_lite.bready <= 0;
        assert_equal(axi_lite.bresp, axi::RESP_OKAY, "Failed to write register");
    endtask
endmodule
