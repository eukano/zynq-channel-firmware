/*
    Copyright 2019 Ethan Reesor

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// queue.sv: Queue implementations

`timescale 1ns / 1ps

// flat_queue: a queue with a single register of storage
module flat_queue #(
        WIDTH = 32
    ) (
        input wire clock,
        input wire resetn,

        queue.slave q
    );

    always @* begin
        q.wready = ~q.rvalid;
    end

    always @(posedge clock or negedge resetn)
        if (~resetn) begin
            q.rvalid <= 0;
            q.rdata <= 'hX;

        end else if (q.wvalid && (q.rready || ~q.rvalid)) begin
            q.rvalid <= 1;
            q.rdata <= q.wdata;

        end else if (q.rready && q.rvalid) begin
            q.rvalid <= 0;
            q.rdata <= 'hX;
        end

endmodule