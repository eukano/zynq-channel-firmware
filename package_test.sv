/*
    Copyright 2019 Ethan Reesor

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// package_test.sv: Packages for testing

`timescale 1ns / 1ps

package test;
    task assert_equal(input logic [31:0] a, b, input string message);
        if ($isunknown(a) || $isunknown(b) || a != b) begin
            $display(message);
            $stop;
        end
    endtask

    task assert_data(input logic [31:0] a, b, input logic [3:0] strobe, input string message);
        automatic reg success = ~$isunknown(a) && ~$isunknown(b) && ~$isunknown(strobe);

        if (strobe[0] && a[ 7: 0] != b[ 7: 0]) success = 0;
        if (strobe[1] && a[15: 8] != b[15: 8]) success = 0;
        if (strobe[2] && a[23:16] != b[23:16]) success = 0;
        if (strobe[3] && a[31:24] != b[31:24]) success = 0;

        if (~success) begin
            $display(message);
            $stop;
        end
    endtask
endpackage