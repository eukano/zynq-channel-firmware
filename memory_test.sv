/*
    Copyright 2019 Ethan Reesor

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// memory_test.sv: Simple implementation of memory for testing with dual AXI ports

`timescale 1ns / 1ps

interface mem #(WIDTH = 0, DEPTH = 0, OFFSET = 0)();
    import axi::*;

    localparam BOUNDARY = 11;
    localparam BYTES = $clog2(WIDTH/8);

    logic [WIDTH-1:0] M [0:(1<<DEPTH)-1];

    function logic in_bounds(input [31:0] address);
        in_bounds = address[BYTES-1:0] == 0 && address[31:DEPTH+BYTES] == OFFSET[31:DEPTH+BYTES];
    endfunction

    function logic [31:0] get(input [31:0] address);
        get = in_bounds(address) ? M[address[BYTES+:DEPTH]] : 'hX;
    endfunction

    task set(input [31:0] address, value);
        if (in_bounds(address))
            M[address[BYTES+:DEPTH]] = value;
    endtask

    task set_strobe(input [31:0] address, value, input [3:0] strobe);
        set(address, apply_strobe(get(address), value, strobe));
    endtask
endinterface

module axi_memory_read_port(
        input wire clock,
        input wire resetn,
        axi_full.slave port,
        mem mem
    );

    reg [31:0] addr, last, len;

    initial #0.001 forever begin
        port.arready <= 1;
        port.rvalid <= 0;
        port.rlast <= 0;
        port.rdata <= 0;
        if (~resetn) @(posedge resetn);

        while (~port.arbeat())
            @(posedge clock or negedge resetn);
        if (~resetn) continue;

        port.arready <= 0;
        addr = port.araddr;
        last = port.arlastaddr();
        len = port.arburstlen();

        if ($isunknown(addr) || $isunknown(last) || $isunknown(len))
            $stop;

        if (port.arburst != axi::BURST_INCR || port.arsize < axi::BURST_32_BIT || addr[mem.BYTES-1:0] != 0 || addr[31:mem.BOUNDARY] != last[31:mem.BOUNDARY]) begin
            // fixed and wrap bursts are not supported
            // less than bus sized bursts are not supported
            // unaligned reads are not supported
            // bursts that cross 2kB boundaries are not supported
            port.read_response(1, 'hX, axi::RESP_SLVERR, 1); #0.001

            while (resetn && port.rvalid) begin
                @(posedge clock or negedge resetn);
                if (port.rbeat()) port.rvalid = 0;
            end
            continue;
        end

        while (len > 0) begin
            len = len - 4;
            port.read_response(1, mem.get(addr), axi::RESP_OKAY, len <= 0); #0.001
            addr = addr + 4;

            while (resetn && port.rvalid) begin
                @(posedge clock or negedge resetn);
                if (port.rbeat()) port.rvalid = 0;
            end
            if (~resetn) break;
        end
    end

endmodule

module axi_memory_write_port(
        input wire clock,
        input wire resetn,
        axi_full.slave port,
        mem mem
    );

    reg err;
    reg [31:0] addr, last, len;

    task error();
        while (~port.wlast || ~port.wbeat())
            @(posedge clock or negedge resetn);

        if (resetn) begin
            port.write_response(1, axi::RESP_SLVERR); #0.001

            while (resetn && port.bvalid) begin
                @(posedge clock or negedge resetn);
                if (port.bbeat()) port.bvalid = 0;
            end
        end
    endtask

    initial #0.001 forever begin
        port.awready = 1;
        port.wready = 0;
        port.bvalid = 0;

        if (~resetn) @(posedge resetn);

        while (resetn && port.awready) begin
            @(posedge clock or negedge resetn);
            if (port.awbeat()) begin
                port.awready = 0;
                addr = port.awaddr;
                last = port.awlastaddr();
                len = port.awburstlen();
                err = 0;

                if ($isunknown(addr) || $isunknown(last) || $isunknown(len)) begin
                    $display("Invalid transaction, signals are unknown!");
                    $stop;
                end

                // fixed and wrap bursts are not supported
                if (port.awburst != axi::BURST_INCR)
                    err = 1;

                // less than bus sized bursts are not supported
                if (port.awsize < axi::BURST_32_BIT)
                    err = 1;

                // unaligned reads are not supported
                if (addr[mem.BYTES-1:0] != 0)
                    err = 1;

                // bursts that cross 2kB boundaries are not supported
                if (addr[31:mem.BOUNDARY] != last[31:mem.BOUNDARY])
                    err = 1;
            end
        end
        if (~resetn) continue;
        if (err) begin
            error();
            continue;
        end

        port.wready = 1;
        while (resetn && len > 0) begin
            len = len - 4;

            @(posedge clock or negedge resetn);
            while (~port.wbeat())
                @(posedge clock or negedge resetn);
            if (~resetn) break;

            if (len > 0 && port.wlast) begin
                $display("Invalid transaction, premature last beat!");
                $stop;
            end

            if (len == 0 && ~port.wlast) begin
                $display("Invalid transaction, overdue last beat!");
                $stop;
            end

            mem.set_strobe(addr, port.wdata, port.wstrb);
            addr = addr + 4;
        end
        if (~resetn) continue;

        port.wready = 0;
        port.write_response(1, axi::RESP_OKAY); #0.001

        while (resetn && port.bvalid) begin
            @(posedge clock or negedge resetn);
            if (port.bbeat()) port.bvalid = 0;
        end
    end
endmodule

module axi_memory (
        input wire clock,
        input wire resetn,

        axi_full.slave A
    );

    mem #(.OFFSET('h12340000), .DEPTH(8), .WIDTH(32)) mem();

    axi_memory_read_port ra(.*, .port(A));
    axi_memory_write_port wa(.*, .port(A));

endmodule

module dual_axi_memory (
        input wire clock,
        input wire resetn,

        axi_full.slave A, B
    );

    mem #(.OFFSET('h12340000), .DEPTH(8), .WIDTH(32)) mem();

    axi_memory_read_port ra(.*, .port(A));
    axi_memory_read_port rb(.*, .port(B));
    axi_memory_write_port wa(.*, .port(A));
    axi_memory_write_port wb(.*, .port(B));

endmodule