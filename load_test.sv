/*
    Copyright 2019 Ethan Reesor

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// load_test.sv: Tests for the load module

`timescale 1ns / 1ps

module load_test();
    reg clock, resetn;

    initial clock = 1;
    always #1 clock = ~clock;

    initial resetn = 0;
    always #10 resetn = 1;

    localparam MEM_OFFSET = aximem.mem.OFFSET;
    localparam DESC_ADDR = 'h120;
    localparam PACK_ADDR = 'h320;
    localparam PACK_CHAN = 'h9;
    localparam PACK_ID = 'hA;
    localparam PACK_LEN = 'h20;
    localparam PACK_STATUS = 'h5A5A5A00;

    axi_full memory();
    queue #(.WIDTH(32)) send_address(), recv_address();
    queue #(.WIDTH(128)) send_descriptor(), recv_descriptor();
    queue #(.WIDTH(32)) send_abort(), recv_abort();

    axi_memory aximem (.*, .A(memory));
    flat_queue #(.WIDTH(32)) send_address_impl(.*, .q(send_address));
    flat_queue #(.WIDTH(32)) recv_address_impl(.*, .q(recv_address));
    flat_queue #(.WIDTH(128)) send_descriptor_impl(.*, .q(send_descriptor));
    flat_queue #(.WIDTH(128)) recv_descriptor_impl(.*, .q(recv_descriptor));
    flat_queue #(.WIDTH(32)) send_abort_impl(.*, .q(send_abort));
    flat_queue #(.WIDTH(32)) recv_abort_impl(.*, .q(recv_abort));

    load load (.*);

    enum {
        RESET,
        TEST_SENDRECV,
        DONE
    } test;
    enum {PUSH, POP1, POP2, END} state;
    reg [95:0] descriptor_bits;

    task fail_now(input string message);
        $display("Test failed: %s", message);
        $stop;
        test <= DONE;
    endtask

    task assert_equal(input logic [31:0] a, b, input string message);
        if (a != b) fail_now(message);
    endtask

    always @(posedge clock or negedge resetn) begin
        send_address.wvalid <= 0;
        recv_address.wvalid <= 0;
        send_descriptor.rready <= 0;
        recv_descriptor.rready <= 0;

        if (~resetn) begin
            {test, state} <= {RESET, PUSH};
            send_address.wvalid <= 0;
            recv_address.wvalid <= 0;

        end else if (test == DONE) begin
            $finish();

        end else if (state == END) begin
            // time between tests
            state <= PUSH;

        end else if (test == RESET) begin
            {test} <= {test + 1};

        end else case (test)
            TEST_SENDRECV: case (state)
                PUSH: begin
                    $display("Starting test of send descriptor");

                    descriptor_bits = driver::descriptor_bits({
                        PACK_ADDR + MEM_OFFSET,
                        PACK_CHAN,
                        PACK_ID,
                        PACK_LEN,
                        PACK_STATUS
                    });

                    aximem.mem.M[DESC_ADDR/4+0] = descriptor_bits[95:64];
                    aximem.mem.M[DESC_ADDR/4+1] = descriptor_bits[63:32];
                    aximem.mem.M[DESC_ADDR/4+2] = descriptor_bits[31: 0];

                    state <= POP1;
                    send_address.push(DESC_ADDR + MEM_OFFSET);
                    recv_address.push(DESC_ADDR + MEM_OFFSET);
                end

                POP1: if (send_descriptor.rvalid) begin
                    state <= POP2;
                    send_descriptor.rready <= 1;

                    assert_equal(send_descriptor.rdata[127:96], DESC_ADDR + MEM_OFFSET, "Descriptor address invalid");
                    assert_equal(send_descriptor.rdata[95:64], PACK_ADDR + MEM_OFFSET, "Packet address invalid");
                    assert_equal(send_descriptor.rdata[63:56], PACK_CHAN, "Packet channel invalid");
                    assert_equal(send_descriptor.rdata[55:48], PACK_ID, "Packet ID invalid");
                    assert_equal(send_descriptor.rdata[47:32], PACK_LEN, "Packet address invalid");
                    assert_equal(send_descriptor.rdata[31: 0], PACK_STATUS, "Packet status invalid");
                end

                POP2: if (recv_descriptor.rvalid) begin
                    {test, state} <= {test+1, END};
                    recv_descriptor.rready <= 1;

                    assert_equal(recv_descriptor.rdata[127:96], DESC_ADDR + MEM_OFFSET, "Descriptor address invalid");
                    assert_equal(recv_descriptor.rdata[95:64], PACK_ADDR + MEM_OFFSET, "Packet address invalid");
                    assert_equal(recv_descriptor.rdata[63:56], PACK_CHAN, "Packet channel invalid");
                    assert_equal(recv_descriptor.rdata[55:48], PACK_ID, "Packet ID invalid");
                    assert_equal(recv_descriptor.rdata[47:32], PACK_LEN, "Packet address invalid");
                    assert_equal(recv_descriptor.rdata[31: 0], PACK_STATUS, "Packet status invalid");

                    $display("Test complete");
                end
            endcase
        endcase
    end
endmodule
