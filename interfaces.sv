/*
    Copyright 2019 Ethan Reesor

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// interfaces.sv: Interface definitions

`timescale 1ns / 1ps

package axi;
    typedef enum logic [1:0] {
        BURST_FIXED = 2'h0,
        BURST_INCR  = 2'h1,
        BURST_WRAP  = 2'h2
    } burst_type;

    typedef enum logic [2:0] {
        BURST_8_BIT    = 3'h0,
        BURST_16_BIT   = 3'h1,
        BURST_32_BIT   = 3'h2,
        BURST_64_BIT   = 3'h3,
        BURST_128_BIT  = 3'h4,
        BURST_256_BIT  = 3'h5,
        BURST_512_BIT  = 3'h6,
        BURST_1024_BIT = 3'h7
    } burst_size;

    typedef enum logic [1:0] {
        RESP_OKAY   = 2'h0,
        RESP_EXOKAY = 2'h1,
        RESP_SLVERR = 2'h2,
        RESP_DECERR = 2'h3
    } response;

    localparam NORMAL_NOCACHE = 'b0011; // modifiable, non-cachable, bufferable

    localparam UNPRIV_SECURE_DATA = 'b000; // unprivileged secure data

    function [15:0] burst_length(input [2:0] size, input [7:0] len);
        burst_length = (1 << size) * (len + 1);
    endfunction

    function [31:0] last_addr(input [31:0] first_addr, input [2:0] size, input [7:0] len);
        last_addr = first_addr + burst_length(size, len) - 1;
    endfunction

    function [31:0] apply_strobe(input [31:0] x, y, input [3:0] strobe);
        apply_strobe[ 7: 0] = strobe[0] ? y[ 7: 0] : x[ 7: 0];
        apply_strobe[15: 8] = strobe[1] ? y[15: 8] : x[15: 8];
        apply_strobe[23:16] = strobe[2] ? y[23:16] : x[23:16];
        apply_strobe[31:24] = strobe[3] ? y[31:24] : x[31:24];
    endfunction

    function [2:0] count_to_strobe(input logic [3:0] strobe);
        count_to_strobe = 0;
        if (strobe[0]) count_to_strobe = count_to_strobe + 1;
        if (strobe[1]) count_to_strobe = count_to_strobe + 1;
        if (strobe[2]) count_to_strobe = count_to_strobe + 1;
        if (strobe[3]) count_to_strobe = count_to_strobe + 1;
    endfunction

    function [3:0] count_to_strobe_ltr(input [2:0] count);
        count_to_strobe_ltr[3] = count > 0;
        count_to_strobe_ltr[2] = count > 1;
        count_to_strobe_ltr[1] = count > 2;
        count_to_strobe_ltr[0] = count > 3;
    endfunction

    function [3:0] count_to_strobe_rtl(input [2:0] count);
        count_to_strobe_rtl[3] = count > 3;
        count_to_strobe_rtl[2] = count > 2;
        count_to_strobe_rtl[1] = count > 1;
        count_to_strobe_rtl[0] = count > 0;
    endfunction
endpackage

package channel;
    typedef enum logic {
        RECEIVE  = 1'b0,
        SEND = 1'b1
    } direction;

    typedef enum logic [2:0] {
        AVAILABLE    = 3'h0, // packet is available
        RETRACTED    = 3'h1, // available packet is retracted
        ACCEPTED     = 3'h2, // available packet is accepted
        REJECTED     = 3'h3, // available packet is rejected
        UPDATED      = 3'h4, // available packet is updated
        COMPLETED    = 3'h5, // accepted packet is completed
        ACKNOWLEDGED = 3'h6  // completed packet is acknowledged
    } message_type;
endpackage

package driver;
    import channel::*;

    localparam TRANSFER_COMPLETE = 1 << 0;
    localparam TRANSFER_ERROR = 1 << 1;

    typedef struct {
        logic [ 7:0] channel;
        logic [ 3:0] unused;
        direction    direction;
        message_type typ;
        logic [15:0] length;
    } message;

    typedef struct {
        logic [31:0] address;
        logic [ 7:0] channel, id;
        logic [15:0] length;
        logic [31:0] status;
    } descriptor;

    localparam WORD_BITS = 32;
    localparam WORD_BYTES = WORD_BITS / 8;

    localparam MESSAGE_BITS = $bits(message);
    localparam DESCRIPTOR_BITS = $bits(descriptor);
    localparam DESCRIPTOR_WORDS = DESCRIPTOR_BITS / WORD_BITS;

    localparam DESCRIPTOR_STATUS_BIT_OFFSET = 64;
    localparam DESCRIPTOR_STATUS_BYTE_OFFSET = DESCRIPTOR_STATUS_BIT_OFFSET / 8;

    function message as_message(input [MESSAGE_BITS-1:0] raw);
        {as_message.channel, as_message.unused, as_message.direction, as_message.typ, as_message.length} = raw;
    endfunction

    function [MESSAGE_BITS-1:0] message_bits(input message msg);
        message_bits = {msg.channel, msg.unused, msg.direction, msg.typ, msg.length};
    endfunction

    function descriptor as_descriptor(input [DESCRIPTOR_BITS-1:0] raw);
        {as_descriptor.address, as_descriptor.channel, as_descriptor.id, as_descriptor.length, as_descriptor.status} = raw;
    endfunction

    function [DESCRIPTOR_BITS-1:0] descriptor_bits(input descriptor desc);
        descriptor_bits = {desc.address, desc.channel, desc.id, desc.length, desc.status};
    endfunction
endpackage

interface queue #(WIDTH = 32)();
    logic wvalid, wready;
    logic rvalid, rready;
    logic [WIDTH-1:0] wdata, rdata;

    modport slave (input wvalid, wdata, rready, output wready, rvalid, rdata);
    modport master (output wvalid, wdata, rready, input wready, rvalid, rdata);
    modport reader (output rready, input rvalid, rdata, import pop);
    modport writer (output wvalid, wdata, input wready, import push);

    function [WIDTH-1:0] pop();
        rready <= 1;
        pop <= rdata;
    endfunction

    task push(input [WIDTH-1:0] data);
        wvalid <= 1;
        wdata <= data;
    endtask
endinterface

interface packet();
    import channel::*;

    logic        pvalid; // packet - valid
    logic        pready; // packet - ready
    logic [ 7:0] pchan;  // packet - channel
    logic [15:0] plen;   // packet - length
    direction    pdir;   // packet - direction
    message_type ptype;  // packet - message type

    modport slave (
        input  pvalid, pchan, plen, pdir, ptype,
        output pready,
        import beat
    );

    modport master (
        output pvalid, pchan, plen, pdir, ptype,
        input  pready,
        import busy, beat
    );

    task send(input logic immediate, input logic [7:0] chan, input logic [15:0] len, input direction dir, input message_type typ);
        if (immediate)
            pvalid <= 1;
        pchan <= chan;
        plen <= len;
        pdir <= dir;
        ptype <= typ;
    endtask

    function busy();
        busy = pvalid && ~pready;
    endfunction

    function beat();
        beat = pvalid && pready;
    endfunction
endinterface

interface stream();
    logic        rxvalid; // receive/header - valid
    logic        rxready; // receive/header - ready
    logic [ 7:0] rxchan;  // receive/header - channel

    logic        ryvalid; // receive/stream - valid
    logic        ryready; // receive/stream - ready
    logic        rylast;  // receive/stream - last
    logic [31:0] rydata;  // receive/stream - data
    logic [ 3:0] rykeep;  // receive/stream - keep

    logic        txvalid; // transmit - valid
    logic        txready; // transmit - ready
    logic [ 7:0] txchan;  // receive/header - channel
    logic        txlast;  // transmit - last
    logic [31:0] txdata;  // transmit - data
    logic [ 3:0] txkeep;  // transmit - keep

    modport slave (
        input  rxvalid, rxchan, ryready,
        output rxready, ryvalid, rylast, rydata, rykeep,
        input  txvalid, txchan, txlast, txdata, txkeep,
        output txready,
        import rysend, rxbeat, rybeat, txbeat
    );

    modport master (
        output rxvalid, rxchan, ryready,
        input  rxready, ryvalid, rylast, rydata, rykeep,
        output txvalid, txchan, txlast, txdata, txkeep,
        input  txready,
        import rxsend, txsend, rxbusy, txbusy, rxbeat, rybeat, txbeat
    );

    modport reader (
        output rxvalid, rxchan, ryready,
        input  rxready, ryvalid, rylast, rydata, rykeep,
        import rxsend, rxbusy, rxbeat, rybeat
    );

    modport writer (
        output txvalid, txchan, txlast, txdata, txkeep,
        input  txready,
        import txsend, txbusy, txbeat
    );

    task rxsend(input logic immediate, input logic [7:0] chan);
        if (immediate)
            rxvalid <= 1;
        rxchan <= chan;
    endtask

    task rysend(input logic immediate, input logic [31:0] data, input logic [3:0] keep, input logic last);
        if (immediate)
            ryvalid <= 1;
        rydata <= data;
        rykeep <= keep;
        rylast <= last;
    endtask

    task txsend(input logic immediate, input logic [7:0] chan, input logic [31:0] data, input logic [3:0] keep, input logic last);
        if (immediate)
            txvalid <= 1;
        txchan <= chan;
        txdata <= data;
        txkeep <= keep;
        txlast <= last;
    endtask

    function rxbusy();
        rxbusy = rxvalid && ~rxready;
    endfunction

    function rybusy();
        rybusy = ryvalid && ~ryready;
    endfunction

    function txbusy();
        txbusy = txvalid && ~txready;
    endfunction

    function rxbeat();
        rxbeat = rxvalid && rxready;
    endfunction

    function rybeat();
        rybeat = ryvalid && ryready;
    endfunction

    function txbeat();
        txbeat = txvalid && txready;
    endfunction
endinterface

interface axi_lite ();
    import axi::*;

    logic        arvalid; // read/addr - valid
    logic        arready; // read/addr - ready
    logic [ 9:0] araddr;  // read/addr - address
    logic [ 2:0] arprot;  // read/addr - protection
    logic        awvalid; // write/addr - valid
    logic        awready; // write/addr - ready
    logic [ 9:0] awaddr;  // write/addr - address
    logic [ 2:0] awprot;  // write/addr - protection
    logic        rvalid;  // read/resp - valid
    logic        rready;  // read/resp - ready
    logic [31:0] rdata;   // read/resp - data
    response     rresp;   // read/resp - response
    logic        wvalid;  // write/data - valid
    logic        wready;  // write/data - ready
    logic [31:0] wdata;   // write/data - data
    logic [ 3:0] wstrb;   // write/data - strobes
    logic        bvalid;  // write/resp - valid
    logic        bready;  // write/resp - ready
    response     bresp;   // write/resp - response

    modport slave (
        input arvalid, araddr, arprot, awvalid, awaddr, awprot, rready, wvalid, wdata, wstrb, bready,
        output arready, awready, rvalid, rdata, rresp, wready, bvalid, bresp,
        import read_response, write_response, get_wdata, rbusy, bbusy, arbeat, awbeat, rbeat, wbeat, bbeat
    );

    modport master (
        output arvalid, araddr, arprot, awvalid, awaddr, awprot, rready, wvalid, wdata, wstrb, bready,
        input arready, awready, rvalid, rdata, rresp, wready, bvalid, bresp,
        import read_address, write_address, write_data, arbusy, awbusy, wbusy, arbeat, awbeat, rbeat, wbeat, bbeat
    );

    task read_address(input logic immediate, input logic [31:0] address, input logic [2:0] protection);
        if (immediate)
            arvalid <= 1;
        araddr <= address;
        arprot <= protection;
    endtask

    task read_response(input logic immediate, input logic [31:0] data, input response response);
        if (immediate)
            rvalid <= 1;
        rdata <= data;
        rresp <= response;
    endtask

    task write_address(input logic immediate, input logic [31:0] address, input logic [2:0] protection);
        if (immediate)
            awvalid <= 1;
        awaddr <= address;
        awprot <= protection;
    endtask

    task write_data(input logic immediate, input logic [31:0] data, input logic [3:0] strobe);
        if (immediate)
            wvalid <= 1;
        wdata <= data;
        wstrb <= strobe;
    endtask

    task write_response(input logic immediate, input response response);
        if (immediate)
            bvalid <= 1;
        bresp <= response;
    endtask

    function [31:0] get_wdata(input [31:0] previous);
        get_wdata = apply_strobe(previous, wdata, wstrb);
    endfunction

    function arbusy();
        arbusy = arvalid && ~arready;
    endfunction

    function arbeat();
        arbeat = arvalid && arready;
    endfunction

    function awbusy();
        awbusy = awvalid && ~awready;
    endfunction

    function awbeat();
        awbeat = awvalid && awready;
    endfunction

    function rbusy();
        rbusy = rvalid && ~rready;
    endfunction

    function rbeat();
        rbeat = rvalid && rready;
    endfunction

    function wbusy();
        wbusy = wvalid && ~wready;
    endfunction

    function wbeat();
        wbeat = wvalid && wready;
    endfunction

    function bbusy();
        bbusy = bvalid && ~bready;
    endfunction

    function bbeat();
        bbeat = bvalid && bready;
    endfunction
endinterface

interface axi_full ();
    import axi::*;

    logic        arvalid; // read/addr - valid
    logic        arready; // read/addr - ready
    logic [31:0] araddr;  // read/addr - address
    burst_type   arburst; // read/addr - burst type
    logic [ 7:0] arlen;   // read/addr - burst length
    burst_size   arsize;  // read/addr - burst size
    logic [ 3:0] arcache; // read/addr - cache
    logic [ 2:0] arprot;  // read/addr - protection
    logic        awvalid; // write/addr - valid
    logic        awready; // write/addr - ready
    logic [31:0] awaddr;  // write/addr - address
    burst_type   awburst; // write/addr - burst type
    logic [ 7:0] awlen;   // write/addr - burst length
    burst_size   awsize;  // write/addr - burst size
    logic [ 3:0] awcache; // write/addr - cache
    logic [ 2:0] awprot;  // write/addr - protection
    logic        rvalid;  // read/resp - valid
    logic        rready;  // read/resp - ready
    logic        rlast;   // read/resp - last
    logic [31:0] rdata;   // read/resp - data
    response     rresp;   // read/resp - response
    logic        wvalid;  // write/data - valid
    logic        wready;  // write/data - ready
    logic        wlast;   // write/data - last
    logic [31:0] wdata;   // write/data - data
    logic [ 3:0] wstrb;   // write/data - strobes
    logic        bvalid;  // write/resp - valid
    logic        bready;  // write/resp - ready
    response     bresp;   // write/resp - response

    modport slave (
        output arready, awready, rvalid, rlast, rdata, rresp, wready, bvalid, bresp,
        input arvalid, araddr, arburst, arlen, arsize, arcache, arprot, awvalid, awaddr, awburst, awlen, awsize, awcache, awprot, rready, wvalid, wlast, wdata, wstrb, bready,
        import read_response, write_response, get_wdata, arburstlen, arlastaddr, awburstlen, awlastaddr, rbusy, bbusy, arbeat, awbeat, rbeat, wbeat, bbeat
    );

    modport master (
        input arready, awready, rvalid, rlast, rdata, rresp, wready, bvalid, bresp,
        output arvalid, araddr, arburst, arlen, arsize, arcache, arprot, awvalid, awaddr, awburst, awlen, awsize, awcache, awprot, rready, wvalid, wlast, wdata, wstrb, bready,
        import read_address, write_address, write_data, arbusy, awbusy, wbusy, arbeat, awbeat, rbeat, wbeat, bbeat
    );

    modport reader (
        input arready, rvalid, rlast, rdata, rresp,
        output arvalid, araddr, arburst, arlen, arsize, arcache, arprot,
        output rready,
        import read_address, arbusy, arbeat, rbeat
    );

    modport writer (
        input awready, wready, bvalid, bresp,
        output awvalid, awaddr, awburst, awlen, awsize, awcache, awprot,
        output wvalid, wlast, wdata, wstrb, bready,
        import write_address, write_data, awbusy, wbusy, awbeat, wbeat, bbeat
    );

    task read_address(input logic immediate, input logic [31:0] address, input burst_type burst, input burst_size size, input logic [8:0] length, input logic [3:0] cache, input logic [2:0] protection);
        if (immediate)
            arvalid <= 1;
        araddr <= address;
        arburst <= burst;
        arsize <= size;
        arlen <= length - 1;
        arcache <= cache;
        arprot <= protection;
    endtask

    task read_response(input logic immediate, input logic [31:0] data, input response response, input logic last);
        if (immediate)
            rvalid <= 1;
        rdata <= data;
        rresp <= response;
        rlast <= last;
    endtask

    task write_address(input logic immediate, input logic [31:0] address, input burst_type burst, input burst_size size, input logic [8:0] length, input logic [3:0] cache, input logic [2:0] protection);
        if (immediate)
            awvalid <= 1;
        awaddr <= address;
        awburst <= burst;
        awsize <= size;
        awlen <= length - 1;
        awcache <= cache;
        awprot <= protection;
    endtask

    task write_data(input logic immediate, input logic [31:0] data, input logic [3:0] strobe, input logic last);
        if (immediate)
            wvalid <= 1;
        wdata <= data;
        wstrb <= strobe;
        wlast <= last;
    endtask

    task write_response(input logic immediate, input response response);
        if (immediate)
            bvalid <= 1;
        bresp <= response;
    endtask

    function [31:0] get_wdata(input [31:0] previous);
        get_wdata = apply_strobe(previous, wdata, wstrb);
    endfunction

    function [15:0] arburstlen();
        arburstlen = burst_length(arsize, arlen);
    endfunction

    function [31:0] arlastaddr();
        arlastaddr = last_addr(araddr, arsize, arlen);
    endfunction

    function [15:0] awburstlen();
        awburstlen = burst_length(awsize, awlen);
    endfunction

    function [31:0] awlastaddr();
        awlastaddr = last_addr(awaddr, awsize, awlen);
    endfunction

    function arbusy();
        arbusy = arvalid && ~arready;
    endfunction

    function arbeat();
        arbeat = arvalid && arready;
    endfunction

    function awbusy();
        awbusy = awvalid && ~awready;
    endfunction

    function awbeat();
        awbeat = awvalid && awready;
    endfunction

    function rbusy();
        rbusy = rvalid && ~rready;
    endfunction

    function rbeat();
        rbeat = rvalid && rready;
    endfunction

    function wbusy();
        wbusy = wvalid && ~wready;
    endfunction

    function wbeat();
        wbeat = wvalid && wready;
    endfunction

    function bbusy();
        bbusy = bvalid && ~bready;
    endfunction

    function bbeat();
        bbeat = bvalid && bready;
    endfunction
endinterface
