/*
    Copyright 2019 Ethan Reesor

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// store_test.sv: Tests for the store module

`timescale 1ns / 1ps

module store_test();
    reg clock, resetn;

    initial clock = 1;
    always #1 clock = ~clock;

    initial resetn = 0;
    always #10 resetn = 1;

    localparam MEM_OFFSET = aximem.mem.OFFSET;
    localparam DESC_ADDR = 'h120;
    localparam PACK_STATUS = 'h5A5A5A00;

    axi_full memory();
    queue #(.WIDTH(64)) send_status(), recv_status();
    queue #(.WIDTH(32)) send_complete(), recv_complete();

    axi_memory aximem (.*, .s_axi(memory));
    flat_queue #(.WIDTH(64)) send_status_impl(.*, .q(send_status));
    flat_queue #(.WIDTH(64)) recv_status_impl(.*, .q(recv_status));
    flat_queue #(.WIDTH(32)) send_complete_impl(.*, .q(send_complete));
    flat_queue #(.WIDTH(32)) recv_complete_impl(.*, .q(recv_complete));

    store store (.*);

    enum {RESET, SEND, RECV, DONE, FAILED} test;

    task fail_now(input string message);
        $display("Test failed: %s", message);
        test <= FAILED;
        $stop;
    endtask

    task assert_equal(input logic [31:0] a, b, input string message);
        if (a != b) fail_now(message);
    endtask

    initial begin
        test <= RESET;
        send_status.wvalid <= 0;
        recv_status.wvalid <= 0;
        send_complete.rready <= 0;
        recv_complete.rready <= 0;
        @(posedge resetn);

        $display("Testing send status");
        test <= SEND;
        aximem.mem.M[DESC_ADDR/4+'h02] = 0;
        send_status.push({DESC_ADDR + MEM_OFFSET, PACK_STATUS | driver::TRANSFER_COMPLETE});
        @(posedge clock) send_status.wvalid <= 0;

        while (~send_complete.rvalid)
            @(posedge clock);

        send_complete.pop();
        assert_equal(send_complete.rdata, DESC_ADDR + MEM_OFFSET, "Descriptor address invalid");
        assert_equal(aximem.mem.M[DESC_ADDR/4+'h02], PACK_STATUS | driver::TRANSFER_COMPLETE, "Packet status invalid");
        @(posedge clock) send_complete.wvalid <= 0;

        $display("Testing recv status");
        test <= RECV;
        aximem.mem.M[DESC_ADDR/4+'h02] = 0;
        recv_status.push({DESC_ADDR + MEM_OFFSET, PACK_STATUS | driver::TRANSFER_ERROR});
        @(posedge clock) recv_status.wvalid <= 0;

        while (~recv_complete.rvalid)
            @(posedge clock);

        recv_complete.pop();
        assert_equal(recv_complete.rdata, DESC_ADDR + MEM_OFFSET, "Descriptor address invalid");
        assert_equal(aximem.mem.M[DESC_ADDR/4+'h02], PACK_STATUS | driver::TRANSFER_ERROR, "Packet status invalid");
        @(posedge clock) recv_complete.wvalid <= 0;

        test <= DONE;
        $display("Tests complete");
        $finish();

    end

endmodule
