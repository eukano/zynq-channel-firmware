/*
    Copyright 2019 Ethan Reesor

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// store.sv: Writes packet descriptors to memory

`timescale 1ns / 1ps

interface store_intr();
    logic valid, ready, send_done, recv_done;
    modport slave(output valid, send_done, recv_done, input ready);
endinterface

module store (
        input wire clock,
        input wire resetn,

        store_intr intr,

        axi_full.writer memory,          // AXI Master
        queue.reader    q_send_status,   // Send Status Queue
        queue.reader    q_recv_status,   // Receive Status Queue
        queue.writer    q_send_complete, // Send Complete Queue
        queue.writer    q_recv_complete  // Receive Complete Queue
    );

    enum {NONE, SEND, RECV} last;

    reg [31:0] addr;
    wire [31:0] send_addr, send_value, recv_addr, recv_value;
    assign {send_addr, send_value} = q_send_status.rdata;
    assign {recv_addr, recv_value} = q_recv_status.rdata;

    task next();
        automatic logic send_ready = q_send_status.rvalid && q_send_complete.wready;
        automatic logic recv_ready = q_recv_status.rvalid && q_recv_complete.wready;
        automatic logic [31:0] address, value;

        if (send_ready && (last != SEND || ~recv_ready)) begin
            last <= SEND;
            address = send_addr;
            value = send_value;
            q_send_status.pop();
        end else if (recv_ready) begin
            last <= RECV;
            address = recv_addr;
            value = recv_value;
            q_recv_status.pop();
        end

        if (send_ready || recv_ready) begin
            addr <= address;
            memory.write_address(1, address + driver::DESCRIPTOR_STATUS_BYTE_OFFSET, axi::BURST_INCR, axi::BURST_32_BIT, 1, axi::NORMAL_NOCACHE, axi::UNPRIV_SECURE_DATA);
            memory.write_data(1, value, 4'hF, 1);
            memory.bready <= 1;
        end
    endtask

    always @(posedge clock or negedge resetn) begin
        q_send_status.rready <= 0;
        q_recv_status.rready <= 0;
        q_send_complete.wvalid <= 0;
        q_recv_complete.wvalid <= 0;

        if (~resetn) begin
            intr.valid <= 0;
            intr.send_done <= 0;
            intr.recv_done <= 0;

        end else if (intr.valid && intr.ready) begin
            intr.send_done <= 0;
            intr.recv_done <= 0;
        end

        if (~resetn) begin
            memory.awvalid <= 0;
            memory.wvalid <= 0;
            memory.bready <= 0;

        end else begin
            if (~memory.awvalid && ~memory.wvalid && ~memory.bready)
                next();

            if (memory.awbeat())
                memory.awvalid <= 0;

            if (memory.wbeat())
                memory.wvalid <= 0;

            if (memory.bbeat()) begin
                memory.bready <= 0;

                if (memory.bresp == axi::RESP_OKAY) begin
                    intr.valid <= 1;
                    if (last == SEND) begin
                        intr.send_done <= 1;
                        q_send_complete.push(addr);
                    end else begin
                        intr.recv_done <= 1;
                        q_recv_complete.push(addr);
                    end
                end
                    // TODO else something
            end
        end
    end

endmodule