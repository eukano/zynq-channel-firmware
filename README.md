# DMA Channels

  * Designed for Go
  * Use AXI DMA, wrap to make semantics easier, possibly register-bang from PL
  * Transfers include a header, with a source field etc
  * Should be usable for transmitting any kind of packets
  * Roughly the same features as LabVIEW's DMA FIFOs?
  * Generic and minimal, useful for general PL-to-PS-user-space
  * Use DMA regions and memory-mapping for max efficiency
  * Maybe a custom bus that's not quite AXI-Stream and not quite AXI-Lite?

## TODO

  * Require acknowledgement before final send/receive, to facilitate non-buffered channel equivalents?
    + A clearable status bit for the descriptor address (each per send and receive)
    + A settable status bit to acknowledge the transfer (each per send and receive)
    + A register for the descriptor address requesting acknowledgement (each per send and receive)
    + A settable bit to cancel the transfer? (each per send and receive)
    + Does it make sense unless it happens after the initial, "Can you receive/produce this packet?"

## Protocol

Packet busses and AXI Streams

  * Master initiated packet
    * (Master) Packet available
    * (Slave) Packet accepted
  * Slave initiated packet
    * (Slave) Packet available
    * (Core) Generate interrupt
    * (Master) Packet accepted
  * Transmit
    * (Master) Prepare descriptor
    * (Core) Process descriptor
    * (Core) Read from memory
    * (Slave) Accept data stream
  * Receive
    * (Master) Prepare descriptor
    * (Core) Process descriptor
    * (Slave) Provide data stream
    * (Core) Write to memory

Packets are uniquely identified with channel and ID. Once a packet is available and accepted,
it is considered active. The bus master may initiate transfer of any active packet.

### Packet

  * Channel, ID, length, and status
  * Statuses are available, accepted, updated
  * Possible statuses are rejected, unavailable

## Driver

  * Character device?
  * Memory map DMA regions via the character device?
  * Open exclusive = I want all transfers
  * Open non-exclusive requires a filter (something to do with the header)
  * Driver receives transfers in background if able, writing to (appropriate) DMA region
  * Driver informs process that data is available

## Firmware

### PL to PS

  1. PL informs PS that transfer is ready via interrupt
  2. PS queries PL to determine what the max transfer size should be (?) or maybe it just picks a number
  3. PS informs PL arbiter that it should accept a transfer
  4. PL arbiter informs data source that it may commence
  5. Transfer
  6. PL informs PS that transfer is complete

### PS to PL

  1. PL informs PS that transfer is ready via interrupt
  2. PS informs PL arbiter that it should expect a transfer of some size
  3. PL arbiter informs data sink that a transfer is ready
  4. Data sink informs PL arbiter that it is ready
  5. Transfer
  6. PL informs PS that transfer is complete


## Implementation

  * Packet Transfer Descriptor
    * Address - starting address of transfer
    * Packet Length - length per packet
    * Packet Count? - number of packets - 1
    * Private (driver data)
      * `struct list_head node` - for storing in linked lists
  * Firmware
    * Registers
      * Control?
      * Status?
      * Descriptor Queues (w/ relevant interrupts on empty/non-empty/threshold)
        * Pending Receipts
        * Completed Receipts
        * Pending Transmissions
        * Completed Transmissions
    * Internally: read from pending, read descriptor from memory, write descriptor to another queue
  * Driver
    * Descriptor Queues
      * Pending - transfer requests not yet completed
      * Completed - enqueue to completed on interrupt, handle in back half
    * Write descriptor address to register, add to pending, wait for interrupt, add to completed, schedule back half, process completed
